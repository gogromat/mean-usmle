# USMLE Drugs

## Mongo

[Mongo shell](http://docs.mongodb.org/v2.2/tutorial/getting-started-with-the-mongo-shell/)
<pre>
In terminal:
> mongo
> show dbs
> use ud 
> db.users.find()

</pre>

#### Start/Stop mongodb
```sudo service mongodb [stop | start | restart]```  

#### Mongodb web / browser api
- ```sudo mongod --rest``` 
- Then go to http://localhost:28017/DATABASE_NAME/COLLECTION_NAME/
- For example: http://localhost:28017/ud/users/


#### Importing/ Exporting Data with [MongoExport](http://docs.mongodb.org/v2.2/reference/mongoexport/) and [MongoImport](http://docs.mongodb.org/manual/reference/program/mongoimport/), [Related Question](http://stackoverflow.com/questions/9762268/import-data-into-openshift-mongodb)

- Locally
  - Export  
    ```mongoexport --db DB_NAME --collection COLLECTION_NAME --out FILE_NAME.json```  
  - Import
    ```mongoimport --db DB_NAME --collection COLLECTION_NAME --type json --file FILE_NAME.json```


- On OpenShift
  - Export
    ```mongoexport --host $OPENSHIFT_MONGODB_DB_HOST --port $OPENSHIFT_MONGODB_DB_PORT --db admin --collection COLLECTION_NAME --username $OPENSHIFT_MONGODB_DB_USERNAME --password $OPENSHIFT_MONGODB_DB_PASSWORD --out FILE_NAME.json```

  - Import
    ```mongoimport --headerline --type json --host $OPENSHIFT_MONGODB_DB_HOST --port $OPENSHIFT_MONGODB_DB_PORT --db admin --collection COLLECTION_NAME --username $OPENSHIFT_MONGODB_DB_USERNAME --password $OPENSHIFT_MONGODB_DB_PASSWORD --file ../data/FILE_NAME.json```

## Node Dependencies

- if you forgot to add ```--save-dev``` to dependencies lookup them through:  
```npm list --depth=0``` to add them manually.


## NodeJS and MongoDB setup instructions

[Introduction to the MEAN Stack, Part I: Setting up your Tools](http://thecodebarbarian.com/2013/07/22/introduction-to-the-mean-stack-part-one-setting-up-your-tools/)  
[Introduction to the MEAN Stack, Part II: Building and Testing a To-do List](http://thecodebarbarian.wordpress.com/2013/07/29/introduction-to-the-mean-stack-part-two-building-and-testing-a-to-do-list/)


# OpenShift-ing

## Notes before updating OpenShift

### Before updating OpenShift
- run database migrations (the db stays intact (does not get deleted), so no worries there)
ex.:
```
openshift $> mongo 
> use admin
> db.drugs.find({}).exec(function(){... db.drugs.save(); })
> exit
```
- copy the images out, since they are not in the /data folder and will get replaces with the local ones. 
  (use SSH for file copying)


## Setup Ruby, RVM, Ruby Gems

- Beautiful instruction to setup it all:
  [http://gorails.com](GoRails) | [For Ubuntu 13.10](http://gorails.com/setup/ubuntu/13.10)

## RHC

- Look for a large list below
- To connect fast to openshift project, type 
 ```rhc app ssh PROJECT_NAME```

## Git & OpenShift

[Explanation](http://stackoverflow.com/questions/12657168/can-i-use-my-existing-git-repo-with-openshift)

Your remote repo is stored with the alias "origin" (the default alias used by git if you clone). You then add the openshift repo as remote to your clone. You do that while explicitly using an alias for the remote repo you "openshift" as alias:
  
``` git remote add openshift -f <openshift-git-repo-url> ```
  
In order to then be able to push the code from your local git repo to openshift you first have to merge your openshift repo with your local bitbucket/github clone. You do that by issuing locally:
  
``` git merge openshift/master -s recursive -X ours ```
  
With this command you tell git to merge the master branch in the openshift git repo with your local git repo. You tell it to merge using the recursive merging strategy and to choose your ("ours") version when there are conflicts.
  
Once the merge is executed you're ready to push your git repo to openshift. You do that by doing:
  
``` git push openshift HEAD ```
  
You tell git to push your local code to the HEAD branch on the remote repo called "openshift" (the alias we stored the openshift git repo at, some paragraphs further up).
  
## Before Deploying
  
- [If you run in file other that server.js](http://javascriptplayground.com/blog/2012/04/node-js-a-todo-app-with-express/)  
Basically solution is to change package.js "main" to:
``` "main" : "MY MAIN FILE" ```
[Similar](https://coderwall.com/p/wmyqbw)

- Change from nodemon to node


## Debugging OpenShift
  
- List of [Common Commands (on SSH connection)](https://www.openshift.com/developers/remote-access) for remote access via rhc  

- [ENVIRONMENT_VARIABLES](https://access.redhat.com/site/documentation/en-US/OpenShift_Online/2.0/html/User_Guide/sect-Environment_Variables.html)

<pre>
OPENSHIFT_APP_DNS   - [APPLICATION_NAME]-[DOMAIN_NAME].example.com
OPENSHIFT_APP_NAME 
OPENSHIFT_APP_UUID  - 0123456789abcdef0123456789abcdef
OPENSHIFT_[CARTRIDGE_NAME]_IP
OPENSHIFT_[CARTRIDGE_NAME]_PORT
OPENSHIFT_SECRET_TOKEN
OPENSHIFT_HOMEDIR   - /var/lib/openshift/51774b763817c83ddb00009d/
OPENSHIFT_DATA_DIR 	- $OPENSHIFT_HOMEDIR/app-root/data/	 A persistent data directory
OPENSHIFT_[CARTRIDGE_NAME]_LOG_DIR - $OPENSHIFT_HOMEDIR/[CARTRIDGE_NAME]/logs/	 Where cartridge-specific logs are stored
OPENSHIFT_[DATABASE_NAME]_DB_LOG_DIR - $OPENSHIFT_HOMEDIR/[DATABASE_NAME]/logs/	 Where database-specific logs are stored
OPENSHIFT_REPO_DIR - $OPENSHIFT_HOMEDIR/app-root/runtime/repo/	 Repository containing the currently deployed version of the application
OPENSHIFT_TMP_DIR - /tmp/
</pre>

- RHC Commands  
  
http://[APPLICATION_NAME]-[DOMAIN_NAME].example.com  

<pre>
rhc tail -a APPLICATION_NAME

rhc setup  - automatic OpenShift/application setup

rhc apps

rhc git-clone APPLICATION_NAME

rhc app
		[create]
		[status  -a 	APPLICATION_NAME]
		[stop 	 -a 	APPLICATION_NAME]
		[start 	 -a 	APPLICATION_NAME]
		[force-stop -a 	APPLICATION_NAME]
		[restart -a 	APPLICATION_NAME]
		[delete 		APPLICATION_NAME]
		[ssh 			APPLICATION_NAME]

	[--help, -s / --scaling ]

rhc accout - list account details

rhc logout - logs user off from SSH session

rhc cartridge 
		[list]
		[add CARTRIDGE_NAME -a APPLICATION_NAME]
		[add CARTRIDGE_NAME -a APPLICATION_NAME -g medium]
		[scale CARTRIDGE_NAME -a hybrid --min 1 --max 10]

		[ACTION CARTRIDGE_NAME -a APPLICATION_NAME]
			[where ACTION 
				(list | add | remove | stop | start | 
				 restart | status | reload | show | storage | scale)]

		[storage --show -a APPLICATION_NAME]
		[storage --show -a APPLICATION_NAME -c CARTRIDGE_NAME]
		[storage CARTRIDGE_NAME -a APPLICATION_NAME ...]
												[--add 3gb | --set 5gb | --remove 5gb]

	[-g / --gear-size (small | medium | large)]

rhc domain 
		[create 					DOMAIN_NAME],
		[list]
		[show -n 					DOMAIN_NAME]
		[rename OLD_DOMAIN_NAME NEW_DOMAIN_NAME]
		[delete 					DOMAIN_NAME]

rhc alias
		[add 	APPLICATION_NAME ALIAS]
		[list 	APPLICATION_NAME]
		[remove APPLICATION_NAME ALIAS]

rhc member
		[add MEMBER_EMAIL_LOGIN -n 		DOMAIN_NAME]
		[remove -n DOMAIN_NAME 	 MEMBER_EMAIL_LOGIN]
		[list 						  	DOMAIN_NAME]
	
	[-n, --role  (view | edit | admin)]



</pre>

- Node Application Structure

<pre>

- node_modules/	Node modules packaged with the application.
- deplist.txt	Deprecated list of npm modules to install with the application.
- npm_global_module_list	 List of globally installed and available npm modules.
- server.js	    OpenShift Online sample Node application.
- package.json	Package descriptor for npm.
- README	    Information about using Node.js with OpenShift Online .
- .openshift/	Location for OpenShift Online specific files.
- .openshift/action_hooks/pre_build	 	Script that runs every git push, but before the build.
- .openshift/action_hooks/build	 	 	Script that runs every git push during the build process (on the CI system).
- .openshift/action_hooks/deploy	 	Script that runs every git push after the build, but before the application is restarted.
- .openshift/action_hooks/post_deploy 	Script that runs every git push, but after the application is restarted.

When you git push, everything in your remote repository directory is recreated. Place items that you want to persist between pushes, such as a database, in the ../data directory on the gear where your OpenShift Online application is running.

</pre>

- [Cron Jobs](https://access.redhat.com/site/documentation/en-US/OpenShift_Online/2.0/html/User_Guide/Scheduling_Timed_Jobs_with_Cron.html)
- [Ports](https://access.redhat.com/site/documentation/en-US/OpenShift_Online/2.0/html/User_Guide/sect-Available_Ports_for_Binding_Applications.html)






## SSH-ing 
  

```app_root/repo``` - where git files are  
```app_root/data``` - persistent storage folder 


### [Copying Files(http://www.mballem.com/en/post/openshift-copy-files-to-your-pc)

- To copy from OpenShift to Local machine:
  
```scp PROJECT_HASH@PROJECT_URL:~/app-root/data/*.json /LOCAL/FOLDER/DESTINATION```


  
### PuTTY
  
- install PuTTY (Software Center)
  
- You have to convert SSH key from OpenSSH to PuTTY format (using PuTTYgen)
[Instructions](https://www.openshift.com/blogs/access-your-application-gear-using-ssh-with-putty) | [convert openSSH to PuTTY](http://webdevbyjoss.blogspot.com/2012/04/convert-openssh-keys-to-putty-on-linux.html)
  
Solution:  
```# apt-get install putty-tools```   
```$ cd ~/.ssh/```  
```$ puttygen id_rsa -o id_rsa.ppk```  


```rsync --ignore-existing config.js 52d5a6975004463d130000c7@ud-gogromat.rhcloud.com:/app_root/repo/config.js```

```rsync -avz --ignore-existing config.js 52d5a6975004463d130000c7@ud-gogromat.rhcloud.com:~/app_root/repo/config.js```

```rsync -avz <deployments directory> <ssh-key of your application>@<application url>:~<application name>/repo/deployments```

For example :

-a - archive mode  
-v - increase verbosity  
-z - compress file data  

```rsync -avz /home/nikhil/OpenShift/test/deployments/ pqrs7b6d3cf24ce12345a1ec3c1b7d99@test-testdomain.rhcloud.com:~/test/repo/deployments```














## Config File Sample Code
  
```../data/config.js```


```
var state = 'dev';

config = {};

config.passport = {
    google : {
        CLIENT_SECRET : "W1fWWJxNmFF1pt_LbJsF-beX",
        CLIENT_ID     : "903450511278-4ln0dmrsr004vrir4mq1hqhkp2egof0t.apps.googleusercontent.com",
        REDIRECT_URL  : "http://localhost:3000/auth/google/callback"
    }
}

config.livePassport = {
    google : {
        CLIENT_SECRET : "",
        CLIENT_ID     : "",
        REDIRECT_URL  : ""
    }
}

config.dev = {
    PORT      	: '3000',
    IP          : '127.0.0.1',
    MONGO_URL 	: 'mongodb://127.0.0.1/ud',
    CACHE       : false,
    passport 	: config.passport,
    superuser 	: ""
};

config.prod = {
    PORT      	: process.env.OPENSHIFT_NODEJS_PORT,
    IP          : process.env.OPENSHIFT_NODEJS_IP,
    MONGO_URL 	: process.env.OPENSHIFT_MONGODB_DB_URL,
    CACHE       : true,
    passport 	: config.livePassport,
    superuser 	: ""
};

module.exports = (state === 'prod') ? config.prod : config.dev;
```

## Tree Library

### Building

- ClientJade library is used to build multiple jade templates into one
- allows to use function like ```jade.render``` and ```jade.compile```
- using ```| !{varName}``` allows to nest multi-templates with jade

ex.: 
- ```cd public/javascripts/jsTree```
- ```clientjade ../../../views/partials/tree/* > templates.js```
