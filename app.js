var express              = require('express')
    // json, form
  , bodyParser           = require('body-parser')
    // multi-part (files)
  , busboy               = require('connect-busboy')
  , methodOverride       = require('method-override')
  , cookieParser         = require('cookie-parser')
  , router               = express.Router()
  , favicon              = require('static-favicon')
  , session              = require('express-session')
  , morgan               = require('morgan')
  , errorHandler         = require('errorhandler')
    
  , _                    = require('lodash')
  , async                = require('async')
  , routes               = require('./routes')
  , SpookyProcess        = require("./processes/spookyProcess.js")  
  , flash                = require('connect-flash')
  , portfinder           = require('portfinder')
  
  , logger               = require("./logger.js")

  , mongoose             = require('mongoose')

  // Configuration File
  , config               = require('../data/config')
  
  , jade                 = require('jade')
  , http                 = require('http')
  , path                 = require('path')
  , fs                   = require('fs')

  // Issue server-side requests
  , request              = require('request')
  // Web scraper
  , scraper              = require('scraper')

  // Console colors
  , colors               = require('colors')
  

  // APP
  , app                  = express()


  // Databases
  , mongooseConnection   = mongoose.connect(config.MONGO_URL)
  // Schemas
  , schemas              = require('./schemas.js')(mongoose)
  , User                 = schemas.User
  , Category             = schemas.Category
  , CategoryType         = schemas.CategoryType
  , Drug                 = schemas.Drug
  , Link                 = schemas.Link
  , SchemaHelper         = schemas.SchemaHelper

  , passport             = require("./authentication")(User)

  ;


// req has no body when raw
// req has    body when x-www-form-urlencoded
// req has    body when form-data


// Set base port depending on the server we run on
portfinder.basePort = config.basePort;

app.engine('jade', jade.__express);

app.set('port', config.PORT);
app.set('id',   config.IP  );
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.set('view cache', config.cache);

// Strict Trailing Slash requests ex. users/
//app.set('strict routing', true);
// Local user Time
//app.set('llocal messaging', true);

app.use(favicon());
app.use(morgan());

/* Middleware - order is very important */
// READ MORE: senchalabs.org/connect

// Static folder (stylesheets, javascript, images, etc)
// bower and project files
app.use(express.static(path.join(__dirname, 'public')));
// npm modules
app.use(express.static(path.join(__dirname, 'node_modules')));

// For parsing Body part of request object
// (ex. is a POST request to the server with some data)
app.use("/api/categories/new/batch", bodyParser({
    // Base of 2. 2^10 = 1024. 1kb is 1024 bytes. 1kb * 1024 = 1mb. result * 3 = 3mb
    limit: 1024 * 1024 * 3
}));
app.use("/", bodyParser());

// default options, no immediate parsing
 //"/api/images",
app.use(busboy({
    limits: {
        fileSize: 1024 * 1024 * 10
    }
}));


// Sessions are implemented using cookies, load Cookie Parter First
app.use(cookieParser() );

// Can convert POST/GET requests into PUT/DELETE
// Must come after the bodyparser so that POST Request's body __method
// can be parsed from it
app.use(methodOverride());

// Must start after cookieParser
app.use(session({ secret: 'MY_SUPER_SECRET_KEY_:p' }));

app.use(flash());

// Initialize Passport!
app.use(passport.initialize());
// Use passport.session() middleware, to support persistent login sessions (recommended).
app.use(passport.session());


app.use(function (err, req, res, next) {
    res.status(err.status || 404);
    res.send(err.message);
});

app.use(function(req, res, next) {
    req.getPath = function () {
        return req.originalUrl ? req.originalUrl.substr(1) : "";
    };
    req.getUrl = function() {
        return req.protocol + "://" + req.get('host') + (req.getPath() ? "/" + req.getPath() : "");
    };
    req.getBaseUrl = function () {
        return req.protocol + "://" + config.IP + ":" + config.PORT;
    };
    req.getFullUrl = function (url) {
        return req.getBaseUrl() + url;
    };
    req.getRawCookie = function () {
        return req.headers.cookie;
    };
    req.getRandom = function () {
        // TODO: put in module of its own
        return (Math.random().toString(36)).slice(2, 17); 
    };
    return next();
});


if (config.state == "dev") {
    app.use(errorHandler());
}


// All collection methods except GET
router.put('/api/*',    ensureAuthenticated, getUser);
router.delete('/api/*', ensureAuthenticated, getUser);
// Post ensure when adding, not when using POST as means of
// sending JSON for listing
router.post('/api/*/new',   ensureAuthenticated, getUser);

// All collections with sub-items
router.all('/api/*/*',  ensureAuthenticated, getUser);


// GET methods (Read)
router.get("/", getUser, function (req, res) {
    console.log(
        "Locals:".white,
        res.locals,
        req.query,
        req.query.redirectedFrom
    );
//    res.locals.path = req.session.returnTo || "/";
    res.render("layout");
});

// INDEX page
router.get("/index", function (req, res) {
    res.render("index");
});

// LIST USERS
router.get("/api/users", getUsers, function (req, res, next) {
    if (req.accepts(['html']))
        return res.render("users/index");
    if (req.accepts(['json']))
        return sendSuccessResponse({
            users: res.locals.users
        }, res, next);
    return requestedWithWrongType(req, res, next);
});

// LIST CATEGORIES
router.get("/api/categories", getCategories, getCategoryTypes, function (req, res, next) {
    if (req.accepts(['html']))
        return res.render("categories/index");
    if (req.accepts(['json']))
        return sendSuccessResponse({
            categories: res.locals.categories,
            categoryTypes: res.locals.categoryTypes
        }, res, next);
    return requestedWithWrongType(req, res, next);
});

// LIST CATEGORY TYPES 
router.get("/api/categoryTypes", getCategoryTypes, function (req, res, next) {
    if (req.accepts(['html']))
        return res.render("categoryTypes/index");
    if (req.accepts(['json']))
        return sendSuccessResponse({
            categoryTypes: res.locals.categoryTypes
        }, res, next);
    return requestedWithWrongType(req, res, next);
});

// LIST DRUGS
router.route("/api/drugs")
    .get(function (req, res, next) {
        if (req.accepts(['html']))
            return res.render("drugs/index");
        else if (req.accepts(['json']))
            // todo maybe add json data?
            return sendSuccessResponse({}, res, next);
        return requestedWithWrongType(req, res, next);
    })
    .post(getDrugsTotalCount, setupPagingFilter, function (req, res, next) {

        var filter = res.locals.pagingFilter;

        console.log(
            "::::: RETURNING DRUG FILTER :::::".white,
            JSON.stringify(filter).white,
            "::::: SORT :::::",
            JSON.stringify(filter.sort || { "parents.parent" : 1 }).red
        );

        SchemaHelper.getDrugsAsync({
            cb: function (err, drugs) {
                if (err || !drugs)
                    return sendErrorResponse(err || "No drugs found", res);

                if (req.accepts(['html']))
                    return res.render("drugs/index");
                else if (req.accepts(['json']))
                    return sendSuccessResponse({
                        items: drugs,
                        filter: filter
                    }, res, next);
                return requestedWithWrongType(req, res, next);
            },
            findParams  : filter.find,
            sortParams  : filter.sort || { "parents.parent" : 1 },
            skipParams  : filter.skip,
            limitParams : filter.limit
        });
    });

// GET CATEGORIES THROUGH POST
router.post("/api/categories", function (req, res, next) {

    if (req.accepts(['html'])) {

        res.redirect('/api/categories');
    
    } else if (req.accepts(['json'])) {

        var errorMessage = "Failed to load categories";

        CategoryType.find().lean().exec(function (err, categories) {
            if (err || !categories) {
                req.flash('error', errorMessage);
                return next(err || new Error(errorMessage));
            } 
            return res.json(categories);
        })

    } else requestedWithWrongType(req, res, next);
});


// SAVING IMAGES THROUGH POST
router.post("/api/images/new", function (req, res, next) {

    var errorMessage = 'There was an error saving an image';

    if (req.body.imageUrl) {
        
        // Url from the web

        http.get(req.body.imageUrl, function (request) {
            
            var imageData   = '',
                newFilePath = './public/images/',
                newRandomImageFileName = "";

            // Image and other media are encoded in binary
            request.setEncoding('binary');

            // Read next chunk of image
            request.on('data', function (chunk) {
                imageData += chunk;
            });

            request.on('end', function () {
                (function writeImage () {

                    var newRandomImage = req.getRandom() + ".png",
                        imageWithFolder = "/images/" + newRandomImage,
                        newRandomImageFileName = path.resolve("./public" + imageWithFolder);

                    console.log(newRandomImageFileName, newRandomImage);
                    
                    fs.exists(newRandomImageFileName, function (exists) {
                        
                        if (exists) {
                            return writeImage();
                        }
                        
                        fs.writeFile(newRandomImageFileName, imageData, 'binary', function (err) {
                            if (err) {
                                req.flash('error', errorMessage);
                                return sendErrorResponse(errorMessage || err, res);
                            }
                            console.log('File: ' + newRandomImageFileName + " written! Image:", newRandomImage);
                            sendSuccessResponse({ file: newRandomImage }, res);
                        });
                    });
                })();
            });

        }).on('error', function (e) {
            return sendErrorResponse("Error receiving an image", res);
        });

    } else {
        
        // Image from local drive

        // start parsing the HTTP-POST upload
        req.pipe(req.busboy);

        req.busboy.on('file', function(fieldName, fileStream, filename, encoding, submittedFileMimetype) {

            var supportedMimeTypes = ['image/png', 'image/jpeg', 'image/jpg', 'image/gif'],
                targetFilePath = path.resolve("./public/images/" + filename);

            // console.log("Fieldname file".white);
            // console.log( (targetFilePath).green );
            if (supportedMimeTypes.indexOf(submittedFileMimetype) < 0) {
                return sendErrorResponse("Unsupported image type.", res);
            }

            var fstream = fs.createWriteStream(targetFilePath);
            fileStream.pipe(fstream);
            fstream.on("close", function () {
                return sendSuccessResponse({ file: filename }, res);
            });

        });

        // form error (ie fileupload-cancel)
        req.busboy.on('error', function (err) {
            return sendErrorResponse(err, res);
        });

    }

});


router
    .route("/api/drugs/new")
    // SHOW NEW DRUG
    .get(function (req, res) {
        res.render("drugs/new");
    })
    // CREATE DRUG
    //TODO: make sure category belong to a category type
    .post(function (req, res, next) {

        // res.json(req.body);
        // console.log("BODY---DRUG",  req.body.drug, req.body.returnErrorIfExists);

        var drug = req.body.item,
            ancestorCategories,
            parentCategoryId,
            asyncCategoryFunctions = [],
            returnErrorIfExists = req.body.returnErrorIfExists;

        asyncCategoryFunctions = checkDrugAndSetAncestorsFromParents(drug, res, next);

        async.parallel(
            // Set parent categories for this drug (if these categories were set)
            asyncCategoryFunctions,
            function (err, parents) {
                
                if (err || !parents || parents.length <= 0) {
                    return sendErrorResponse(err || "No categories are found", res);
                }

                // console.log("Final:", parents);
                drug.parents = parents;

                SchemaHelper.getDrugsAsync({
                    cb: saveDrugIfNew,
                    findParams: {
                        "name" : drug.name
                    },
                    findOne: true
                });
                
                function saveDrugIfNew(err, existingDrug) {
                    if (err) {
                        return sendErrorResponse(err || "There was an error finding drug by name", res);                            
                    }
                    // drug exists
                    if (existingDrug) {
                        if (returnErrorIfExists && returnErrorIfExists == true) {
                            return sendErrorResponse("Drug already exists", res);
                        }
                        return sendSuccessResponse({ drug: existingDrug, message: "Drug already exists" }, res);
                    }
                    // drug does not exist
                    new Drug(drug).save(function (saveNewDrugErr, result) {
                        if (saveNewDrugErr || !result) {
                            return sendErrorResponse(["Could not save new drug.", saveNewDrugErr], res);
                        }
                        return sendSuccessResponse({ drug: result }, res);
                    });
                    
                }
                
            }
        );


    });


// CREATE USER
router.post("/api/users/new", function (req, res, next) {
    var formUser = req.body.item,
        errorMessage = 'Could not create user';

    // console.log("FORM USER:", formUser);
    
    new User({
        username : formUser.username, 
        email    : formUser.email
    }).save(function (err, user) {
        if (err || !user) {
            req.flash('error', errorMessage);
            return sendErrorResponse(err || errorMessage, res);
        }

        if (req.accepts(['json']))
            return sendSuccessResponse({ user: user }, res);
        return requestedWithWrongType(req, res, next);
    });
});



// CREATE CATEGORY
router.post("/api/categories/new", function (req, res, next) {

    // console.log("POSTING NEW CATEGORY".blue, req.body);
    // 53641abe95b2cd4710374d7a

        // Current Category Type
    var submittedCategoryType = req.body.categoryType._id || null,
        // Current Parent Category or null (New Top Category)
        submittedCategory = req.body.category._id || null,
        // Category's Ancestor Categories
        ancestorCategories = [],
        categoryNames = req.body.categoryNames,
        categoryData  = req.body.data,
        errorMessage  = "Could not find category Type by categoryType id: " + submittedCategoryType;

    if (!categoryNames)         { return sendErrorResponse("No category names were given.", res); }
    if (!submittedCategoryType) { return sendErrorResponse("No category type was given.", res);   }

    console.log(
        submittedCategory,
        submittedCategoryType,
        categoryNames,
        categoryData,
        errorMessage
    );

    SchemaHelper.getCategoryTypesById(submittedCategoryType, { 
        cb: function (err, parentCategoryType) {

            if (err || !parentCategoryType) {
                return sendErrorResponse(errorMessage, res);
            } 

            // If there is ancestor category, setup ancestor categories
            if (submittedCategory) {
                // Is a sub-category
                parentCategoryType.categories.forEach(function (category_id) {
                    
                    if (category_id == submittedCategory) {

                        // Get the category from category collection
                        SchemaHelper.getCategoriesById(submittedCategory, {
                            cb: function (err, c) {
                                if (err || c == null || !c.ancestors) {
                                    return sendErrorResponse(err || "Could not find appropriate category", res);
                                }

                                // copy existing parent category ancestors
                                ancestorCategories = c.ancestors;
                                // push parent ancestors categories with new categories parent
                                ancestorCategories.push(submittedCategory);
                            },
                            findOne: true
                        });
                    }
                });
            }

            var asyncCallbackFunctions = [];

            // Populate CategoryType with new Categories (names coming from request)
            categoryNames.forEach(function (name) {

                console.log("new name:".green, name);
            
                asyncCallbackFunctions.push(

                    function addNewCategory (cb) {

                        // Find if category name already exists (under particular parent)
                        SchemaHelper.getCategoriesAsync({
                            cb: addCategoryIfNew,
                            findParams: {
                                name: name,
                                parent: submittedCategory
                            },
                            findOne: true
                        }, res);

                        // console.log("Next category name", name);

                        function addCategoryIfNew (err, category) {
                            if (err) {
                                console.log("Could not fetch category by name".red);
                                return cb(err || "Could not fetch category by name");
                            }
                            // category exists
                            else if (category) {
                                // If category exists, there is no need to add it again
                                console.log("already exists");
                                logger.logInfo("Category already exists", category);
                                return cb();
                            }

                            // category does not exist
                            var newCategory = new Category({
                                name: name,
                                parent: submittedCategory,
                                ancestors: ancestorCategories,
                                data: categoryData || {}
                            });
                            newCategory.save(function savedCategoryCallback (err) {
                                console.log("saving new category".green, name);
                                if (err) {
                                    console.log("could not save".red, name);
                                    logger.logError("Could not add new category", err);
                                    return cb(err || "Could not add new category");
                                }
                                console.log("Successfully Added new category".green, newCategory);
                                parentCategoryType.categories.push(newCategory);
                                return cb();
                            });
                            
                        }

                    }
                );


            });


            asyncCallbackFunctions.push(
                function updateParentCategoryTypeWithNewCategories (cb) {
                    parentCategoryType.save(function (err) {
                        if (err) {
                            var errorMessage = "Could not save categories for the parent category type";
                            logger.logError(err || errorMessage);
                            return cb(err || errorMessage);
                        }
                        return cb();
                    });
                }
            );

            async.series(asyncCallbackFunctions,
                function (err) {
                    console.log("returning from series");
                    if (err) {
                        console.log(err);
                        logger.logError(err);
                        return res.json("Could not save new categories");
                    }
                    console.log("success");
                    if (req.accepts(['html'])) {
                        return res.redirect("/api/categories");
                    } else if (req.accepts(['json'])) {
                        return res.send({ success: true });
                    }
                    console.log("wrong content type");
                    return requestedWithWrongType(req, res, next);
                });

        },
        findOne : true,
        lean    : false
    });

});

// CREATE CATEGORY TYPE
router.post("/api/categoryTypes/new", function (req, res, next) {

    var errorMessage = "No Category Type is set.";
    
    if (!req.body.item)
        return sendErrorResponse(errorMessage, res);    

    new CategoryType(req.body.item).save(function (err, result) {
        if (err || !result) 
            return sendErrorResponse(["Could not save new Category Type.", err], res);
        return sendSuccessResponse({ categoryType: result}, res);
    });

});




router
    .route("/api/categories/:id")
    // UPDATE CATEGORY
    .put(function (req, res, next) {

        var item = req.body.item;
        
        // TODO: change to save for consistency
        Category.update({ 
            _id: item._id
        }, { 
            $set: { 
                name : item.name
            }
        }, function (err, result) {
            if (err) return sendErrorResponse(err, res);
            return sendSuccessResponse(res);
        });
    })
    // DELETE CATEGORIES
    // Triggers deletion of category type category id reference
    // and drug parents - the category id reference
    // TODO: try to refactor this mess
    .delete(function (req, res) {

        var asyncFunctions = [],
            asyncDrugFunctions = [];

        function removeSubCategories(category_id, cb) {

            try {

            } catch (e) {
                return sendErrorResponse(err, res);
            }
            var newMongoCategoryId;
            SchemaHelper.getMongoId(category_id, function (err, mongoId) {
                if (err) {
                    return sendErrorResponse("There was an error getting id");
                }
                newMongoCategoryId = mongoId;
            });

            Category
                .find({ parent: newMongoCategoryId })
                .lean()
                .exec(function (err, subCategories) {
                    if (err) {
                        if (cb) return cb(err);
                        return sendErrorResponse(err, res);
                    }  

                    subCategories.forEach(function (subCategory) {
                        removeSubCategories(subCategory._id.toString());
                    });

                    Category.remove({ _id: newMongoCategoryId }, function (err, category) {
                        if (err || !category) {
                            if (cb) return cb(err, category);
                            return sendErrorResponse(err, res);
                        }

                        // Update category types
                        asyncFunctions.push(function (callback) {
                            
                            SchemaHelper.getCategoryTypesAsync({
                                cb: function (err, categoryType) {
                                    
                                    if (err || !categoryType || !categoryType.categories) {
                                        return callback(err, categoryType);
                                    }

                                    categoryType.categories.forEach(function (c, key) {
                                        if (c == category_id) {
                                            categoryType.categories.splice(key, 1);
                                        }
                                    });

                                    CategoryType.update({
                                        _id : categoryType._id
                                    }, {
                                        $set: {
                                            categories: categoryType.categories
                                        }
                                    }, function (err, result) {
                                        callback(err, result);
                                    });
                                    
                                },
                                findParams: {
                                    "categories" : newMongoCategoryId
                                },
                                findOne : true,
                                lean    : true
                            });
                        });

                        // Update drugs
                        asyncFunctions.push(function (callback) {

                            SchemaHelper.getDrugsAsync({
                                cb: function (err, drugs) {
                                    if (err || !drugs) {
                                        return callack(err, drugs);
                                    }
                                    drugs.forEach(function (drug) {
                                        if (drug.parents) {
                                            drug.parents.forEach(function (parent, key) {
                                                if (parent.parent == category_id) {
                                                    drug.parents.splice(key, 1);
                                                }
                                            });

                                            asyncDrugFunctions.push(function (drugCallback) {
                                                Drug.update({
                                                    _id: drug._id
                                                }, {
                                                    $set: {
                                                        parents: drug.parents
                                                    }
                                                }, function (err, result) {
                                                    drugCallback(err, result);
                                                });
                                            });
                                        }
                                    });

                                    callback(null);
                                },
                                findParams: {
                                    "parents.parent" : category_id
                                },
                                findOne : false,
                                lean    : true
                            })
                        });

                        if (cb) cb(null, category);
                    
                    });
                
            });
        }

        async.waterfall([
            // First remove categories,
            // and push category types to array of objects to modify
            function (callback) {
                removeSubCategories(req.params.id, function (err, category) {
                    return err ? sendErrorResponse(err, res) : callback(null, category);
                });
            }
        ], function (err, result) {
            // In parallel, update category types
            async.parallel(
                asyncFunctions,
                function (asyncErr, result) {
                    if (asyncErr) return sendErrorResponse(asyncErr, res);

                    // In parallel, update drugs
                    async.parallel(
                        asyncDrugFunctions,
                        function (drugErr, drugResult) {
                            if (drugErr) return sendErrorResponse(drugErr, res);
                            sendSuccessResponse(res);
                        }
                    );
                    
                }
            );
        });
    });


router
    .route("/api/drugs/:id")
    // SHOW DRUG
    .get(function (req, res, next) {
        SchemaHelper.getDrugsAsync({
            cb: function (err, drugs) {

                if (err || !drugs)
                    return sendErrorResponse(err || "No drugs found", res);

                if (req.accepts(['html']))
                    return res.render("drugs/show");

                if (req.accepts(['json']))
                    return sendSuccessResponse({
                        items: drugs
                    }, res, next);

                return requestedWithWrongType(req, res, next);
            },
            findParams: { _id: req.params.id },
            findOne: true,
            localsName: 'drug'
        });
    })
    // UPDATE DRUG
    .put(function (req, res, next) {

        var asyncCategoryFunctions = [];

        Drug.findOne({
        
            _id: req.params.id 

        }, function (findErr, drug) {

            if (findErr || !drug) {
                return sendErrorResponse(findErr || "No such drug found in a system", res);
            }
                
            // copy over the new properties
            _.extend(drug, req.body.item);

            asyncCategoryFunctions = checkDrugAndSetAncestorsFromParents(drug, res, next);

            async.parallel(
                asyncCategoryFunctions,
                function (err, parents) {

                    if (err || !parents) {
                        return sendErrorResponse(err || "No categories are found", res);
                    }

                    console.log("Drug parents:".white, parents);

                    drug.parents = parents;
                    delete drug.__v;

                    // and after all that
                    drug.save(function (saveErr, result) {
                        if (saveErr || !result)
                            return sendErrorResponse(["Could not update drug", saveErr], res);
                        console.log("Saved drug Version:", result.__v, "drug:", drug);                                    
                        return sendSuccessResponse({ drug: result }, res);
                    });
                }
            );
            
        });
    })
    // DELETE DRUGS
    .delete(function (req, res) {
        Drug.remove(
            { _id: req.params.id.toString() },
            function deletingDrugResult (err) {
                if (err) return sendErrorResponse(err, res);
                return sendSuccessResponse(res);
        });
    });


// SHOW PARTIAL FILES
router.get("/partials/:name", function (req, res) {
    res.render('partials/' + req.params.name, { 
        user: req.user 
    });
});


router
    .route("/api/categoryTypes/:id")
    // UPDATE CATEGORY TYPE
    .put(function (req, res, next) {

        var item = req.body.item;
                
        CategoryType.update({
            _id: item._id
        }, {
            $set: {
                name : item.name
            }
        }, function (err, result) {
            if (err || !result) return sendErrorResponse(err || "Could not update category type", res);
            return sendSuccessResponse(res);
        });
    })
    // DELETE CATEGORY TYPES
    // Triggers deletion of categories and drugs
    // TODO: try to refactor this mess
    .delete(function (req, res) {

        var categoryTypeId = req.params.id,
            mongoCategoryTypeId,
            errorMessage = "Could not delete category type";

        SchemaHelper.getMongoId(req.params.id, function (err, mongoId) {
            if (err) return sendErrorResponse("There was an error getting id");
            mongoCategoryTypeId = mongoId;
        });


        SchemaHelper.getCategoryTypesById(req.params.id, {
            cb: function (err, categoryType) {
                    if (err || !categoryType) {
                        return sendErrorResponse(err || "No such category type found", res);
                    }

                    // We have this category type, continue
                    var categoryIds = categoryType.categories,
                        asyncCategoryFunctions = [],
                        asyncDrugFunctions = [],
                        drugParentHasToBeChanged;

                    // Since fetching drugs is async, we have to call it first
                    // in order to add drug functions to asyncDrugFunctions
                    async.waterfall([
                        function (cb) {



                            // Remove all dependent parent (category type) from drugs
                            SchemaHelper.getDrugsAsync({
                                cb: function gotDrugsCallback (err, drugs) {

                                    if (err) return callback(err || "Could not execute query to find drugs");

                                    drugs.forEach(function (drug) {
                                    
                                        drugParentHasToBeChanged = false;
                                        
                                        drug.parents.forEach(function (parent, key) {
                                            if (parent.categoryType == categoryTypeId) {
                                                drugParentHasToBeChanged = true;
                                                drug.parents.splice(key, 1);
                                            }
                                        });

                                        // Only update necessary drugs
                                        if (drugParentHasToBeChanged) {                                            
                                            asyncDrugFunctions.push(function asyncUpdateDrugs (drugCallback) {
                                                Drug.update({
                                                    _id : drug._id
                                                }, { 
                                                    $set: { parents: drug.parents }
                                                }, function resultOfUpdatingDrug (err, result) {
                                                    // console.log("__result__", err, result);
                                                    drugCallback(err, result);
                                                });
                                            });
                                        }
                                    });

                                    cb();
                                },
                                findParams: {
                                    "parents.categoryType": mongoCategoryTypeId
                                },
                                lean: true
                            });
                        }
                    ], function (drugAsyncErr, result2) {

                        // Remove all dependent categories
                        categoryIds.forEach(function(categoryId) {
                            asyncCategoryFunctions.push(
                                function (callback) {
                                    Category.remove({_id: categoryId}, function (err, category) {
                                        // irrelevant if we could not delete category, continue
                                        callback(null, category);
                                    });
                                }
                            );
                        });

                        async.parallel(
                            asyncCategoryFunctions,
                            function (err, result) {
                                if (err)
                                    return sendErrorResponse(err || "There was an error deleting category type", res);

                                async.parallel(
                                    asyncDrugFunctions,
                                    function deleteDrugsInParallelResult (drugErr, drugResult) {
                                        if (drugErr)
                                            return sendErrorResponse(drugErr || "Could not delete drugs");

                                        CategoryType.remove(
                                            { _id: categoryTypeId }, 
                                            function deletingCategoryTypeResult (removeErr, removeCategoryType) {
                                            if (removeErr || !removeCategoryType) {
                                                return sendErrorResponse(removeErr || "No such category type found", res);
                                            }
                                            return sendSuccessResponse(res);
                                        });      
                                    }
                                );   
                            });
                    });
            },
            findOne: true,
            lean: true
        });
    });



router
    .route("/api/users/:id")
    // UPDATE USERS
    .put(function (req, res, next) {

        var formUser = req.body.item,
            errorMessage = "Cannot find user",
            updateError = "Failed to update user",
            authError = "You cannot update admin's email";
        
        console.log("PUTting user", req.params.id.toString(), formUser);

        User.findOne({_id: req.params.id.toString() }, function resultOfFindingUser (err, user) {
            if (err) {
                req.flash('error', err || errorMessage);
                return sendErrorResponse(err || errorMessage, res);
            }

            if (user.email == config.superuser && formUser.email != user.email) {
                req.flash('error', authError);
                return sendErrorResponse(authError, res);
            }

            // todo: change to save later for consistency
            user.update({
                $set: {
                    username : formUser.username,
                    email    : formUser.email 
                }
            }, function resultOfUpdatingUser (err, result) {
                if (err || !result) {
                    req.flash('error', err || updateError);
                    return sendErrorResponse(err || updateError, res);
                }
                return sendSuccessResponse(res);
            });
            
        });
    })
    // DELETE USERS
    .delete(function (req, res) {

        var errorMessage = "There was an error deleting user.";

        SchemaHelper.getUsersById(req.params.id, {
            cb: function getUserById (err, user) {
                
                if (err || !user) {
                    return sendErrorResponse(err || errorMessage, res);
                }
                if (user.email == config.superuser) {
                    return sendErrorResponse("You cannot delete an admin!", res);
                }

                user.remove(function resultOfRemovingUser (err, deletedUser) {
                    if (err)
                        return sendErrorResponse(err || errorMessage, res);
                    return sendSuccessResponse(res);
                });
                
            },
            findOne : true,
            lean    : false
        });

    });




router.post("/api/scrape/wikipedia", function (req, res, next) {
    scrape("wikipedia", req, res, next);
});
router.post("/api/scrape/drugBank", function (req, res, next) {
    scrape("drugBank", req, res, next);
});
router.post("/api/scrape/atc", function (req, res, next) {
    scrapeATC(req, res, next);    
});

router.post("/api/categories/new/batch", function (req, res, next) {

    var categories = req.body.data;

    logger.log("info", "AT: api/categories/new/batch".blue, req.body.data);

    if (!categories) {

        sendErrorResponse("No ATC categories provided", res);

    } else {

        var categoriesArray = [];
        for (var category in categories) {
            categoriesArray.push(categories[category]);
        }

        async.eachSeries(categoriesArray, function (category, callback) {

            var parentName = (categories[category.parent] ? categories[category.parent].name : null),
                categoryParentCode = category.parent;

            if (!parentName || !categoryParentCode) {
                // No parent name or atc code = no parent category to search for
                addNewCategoryOrDrug();

            } else {
                // Find parent category first by either name or atc code

                var findParams;

                if (parentName)         findParams = { name: parentName };
                if (categoryParentCode) findParams = { "data.atc.code":  categoryParentCode };

                // Find parent category by name
                SchemaHelper.getCategoriesAsync({
                    cb: addNewCategoryOrDrug,
                    findParams: findParams
                }, res);
            }

            function addNewCategoryOrDrug (err, parentCategories) {
                if (err || (parentCategories && parentCategories.length > 1))  {
                    // If there is an error or more than one parent categories
                    // logger.logError("Categories Batch: Error or > 1 parent Categories found", categoryParentCode, parentName, err);
                    sendErrorResponse(err || "Categories Batch: Error or > 1 parent Categories found", res);
                    callback(err);
                } else {

                    // If there is one or zero parent categories
                    var parentCategoryId;
                    if (parentCategories && parentCategories.length != 0) {
                        parentCategoryId = parentCategories[0]._id;
                    }

                    if (!category.isDrug || category.isDrug == false || category.isDrug == "false") {
                        // Adding category

                        request({
                            headers : { 
                                "Accept": "application/json",
                                "Cookie" : req.getRawCookie()
                            },
                            url     : req.getFullUrl("/api/categories/new"),
                            method  : "POST",
                            jar     : true,
                            json    : {
                                categoryType : category.categoryType,
                                category     : parentCategoryId,
                                name         : [category.name],
                                data: {
                                    atc : {
                                        code: category.code
                                    }
                                }
                            }
                        }, function savedCategoryCallback (err, response, body) {
                            if (!err && response && response.statusCode == 200 && body && body.success == true) {
                                // console.log("Categories Batch: Saved category".green, category);
                                callback();
                            } else {
                                logger.logError("Categories Batch: Could not save category", category, err);
                                callback(err || "Could not add category");
                            }
                        });

                    } else {
                        // Adding drug
                        var drug = category;
                        drug.parents = [];
                        drug.parents.push({
                            categoryType : drug.categoryType,
                            parent       : parentCategoryId
                        });

                        request({
                            headers : { 
                                "Accept": "application/json",
                                "Cookie" : req.getRawCookie()
                            },
                            url     : req.getFullUrl("/api/drugs/new"),
                            method  : "POST",
                            jar     : true,
                            json    : {
                                item: drug,
                                returnErrorIfExists: false
                            }
                        }, function savedDrugCallback (err, response, body) {
                            if (!err && response && response.statusCode == 200 && body && body.success == true) {
                                // console.log("Categories Batch: Saved drug", drug);
                                callback();
                            } else {
                                logger.logError("Categories Batch: Could not save drug", drug);
                                callback(err || "Could not add drug");
                            }
                        });
                    }
                }


            }
        }, function finalCallback (err) {
            if (err)
                return sendErrorResponse(err || "Failed saving ATC Categories and Drugs", res);
            return sendSuccessResponse(res);
        });
    

    }
});

function scrapeATC (req, res, next) {
    
    var ATCSetup = {
            content: "#content",
            linkWithCode: "a[href^='./?code=']",
            categoryType: req.body.categoryType,
            getURLFromATCCode: function (atcCode) {
                return "http://www.whocc.no/atc_ddd_index/?code=" + atcCode;
            },
            generateFirtLevelATCCategories: function () {
                req.body.codes.forEach(function (code) {
                    ATCCategories[code.code] = code;
                    ATCSetup.addCategoryTypeToCategory(ATCCategories[code.code]);
                });
            },
            addCategoryTypeToCategory: function (category) {
                category.categoryType = ATCSetup.categoryType;
            }
        },
        ATCCategories = {};

    ATCSetup.generateFirtLevelATCCategories();
    visitUnvisitedATCCodeLinks();

    function sendNewATCCodesCategoriesAndDrugsToSave () {
        
        request({
            url     : req.getFullUrl("/api/categories/new/batch"),
            method  : "POST",
            headers : { 
                "Accept" : "application/json",
                "Cookie" : req.getRawCookie()
            },
            json    : { 
                data: ATCCategories
                //categoryType
            }

        }, function callback (err, response, body) {
            if (err || response.statusCode != 200 || !body || !body.success == true) {
                return sendErrorResponse(err || "Error: /scrape/atc - saving ATC code categories and drugs", res);
            }
            // logger.log("Success: /scrape/atc");
            sendSuccessResponse({
                message: "Successfully saved ATC code categories and drugs" 
            }, res);
        });
    }

    function visitUnvisitedATCCodeLinks () {
        
        var nothingLeftToVisit = true;

        for (var atcCode in ATCCategories) {
            if (!ATCCategories[atcCode].visited) {
                nothingLeftToVisit = false;
                visitATCCodeLinkAndAddNewATCCodes(atcCode);
                break;
            }
        }

        if (nothingLeftToVisit) {
            sendNewATCCodesCategoriesAndDrugsToSave();
        }
    }

    function getMatchInfoOfATCCode (code) {
        // Given the word, tries to match it against multiple RegExp of ATC codes
        // RegExp is split by levels: ex. V 01 A A 01 - V01AA01
        var letter = code[0],
            regExpLeveled = [letter, "\\d{1,3}", "[A-Z]", "[A-Z]", "\\d{1,3}"],
            buildRegExp   = "",
            matchLevel    = 0,
            matches       = false,
            parentATCCode = "";

            regExpLeveled.forEach(function (level, index) {
                
                buildRegExp += level;
                aLevelMatch = new RegExp("^" + buildRegExp).exec(code);
                
                if (aLevelMatch) {
                    // match exists
                    matches = true;

                    // matches this level
                    matchLevel += 1;

                    if (aLevelMatch[0] != code) {
                        // find parent code
                        parentATCCode = aLevelMatch[0];
                    }
                }
            });

        return {
            match : matches,
            level : matchLevel,
            parent: parentATCCode
        };
    }

    function addNewATCCategoryCodes (categories) {
        
        for (var atcCode in categories) {

            if (!ATCCategories[atcCode]) {
                
                var category   = categories[atcCode],
                    matchInfo  = getMatchInfoOfATCCode(atcCode),
                    matchLevel = matchInfo.level;

                ATCSetup.addCategoryTypeToCategory(category);
                
                category.parent = matchInfo.parent;
                
                // setup atc code data
                category.data = {};
                category.data.atc = {};
                category.data.atc.code = atcCode;
                

                if (matchLevel == 5) {
                    // drug
                    category.isDrug = true;
                } else {
                    // category
                    category.isDrug = false;
                }

                ATCCategories[atcCode] = category;

            // copy name if it is blank (first level)
            } else if (!ATCCategories[atcCode].name) {
                ATCCategories[atcCode].name = category.name;    
            }
        }

        visitUnvisitedATCCodeLinks();
    }

    function visitATCCodeLinkAndAddNewATCCodes (atcCode) {

        if (ATCCategories[atcCode]) {
            // No need to visit this node again, mark as visited
            ATCCategories[atcCode].visited = true;
        }
        // console.log(atcCode);

        scraper(ATCSetup.getURLFromATCCode(atcCode), function (err, $) {


            if (err) {
                logger.logError("Scrape ATC: Could not get ATC Code URL", err);
            } else {
    
                var newATCCategoryCodes = {},
                    links = $(ATCSetup.content).find(ATCSetup.linkWithCode);
                    
                $.each(links, function (i, link) {
                    
                    var url = $(link).attr('href'),
                        isATCCodeMatch = /.*code=([A-Z0-9]{1,7})&?.*/.exec(url),
                        newCode   = (isATCCodeMatch.length >= 1 ? isATCCodeMatch[1] : ""); 
                    
                    if (newCode != "") {
                        newATCCategoryCodes[newCode] = {
                            name: $(link).text(),
                            // drug links (length is 7) are visited
                            visited: (isATCCodeMatch[1].length == 7),
                            href: url,
                            code: newCode
                        };
                    }
                });

                addNewATCCategoryCodes(newATCCategoryCodes);
            }
        });
    }

}




function scrape (process, req, res, next) {

    var data = req.body.data;

    if (!process || !data)
        return sendErrorResponse("Drug name or web address was not privided", res);

    portfinder.getPort(function (err, port) {
        if (err)
            return sendErrorResponse("All ports are taken. Please try again", res);
        new SpookyProcess(process, {
            child: {
                transport: 'http',
                port: port
            },
            callback: function (err, data) {
                if (err)
                    return sendErrorResponse(err || "Failed to scrape data.", res);
                return sendSuccessResponse({ data: data }, res);
            },
            data: data
        });
    });

}

// GET Session Flash Messages
router.get("/api/messages", function (req, res) {
    return res.json({ messages: req.flash() });
});

/*
res.send(req.acceptedLanguages);
*/
router.get("/login", getUser, function (req, res) {
    res.render('auth/login');
});

router.get("/isLoggedIn", getUser, function (req, res) {
    if (req.isAuthenticated()) {
        return sendSuccessResponse({ isLoggedIn: true }, res);
    }
    res.send(401);
});

router.get("/logout", function (req, res) {
    req.logout();
    res.redirect('/');
});

// GET /auth/google
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  The first step in Google authentication will involve
//   redirecting the user to google.com.  After authorization, Google
//   will redirect the user back to this application at /auth/google/callback
router.get("/auth/google", passport.authenticate("google", { 
        scope: 
            ['https://www.googleapis.com/auth/userinfo.profile', 
             'https://www.googleapis.com/auth/userinfo.email'] 
    }), function (req, res) {
        // The request will be redirected to Google for authentication, 
        // so this function will not be called.
    });

// GET /auth/google/callback
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  If authentication fails, the user will be redirected back to the
//   login page.  Otherwise, the primary route function function will be called,
//   which, in this example, will redirect the user to the home page.

// assignProperty - the request property (req) we assign user upon. 
// Default is 'user'. (req.user). On success, the next() gets called.
router.get("/auth/google/callback", function (req, res, next) {
    passport.authenticate('google', {
        failureFlash    : true,
        failureRedirect : '/login',
        successFlash    : "You have successfully logged in!",
        successRedirect : "/"
        // assignProperty: 'user'
    })(req, res, next);
});

function getUser (req, res, next) {
    res.locals.user = req.user;
    return next();
}

function getUsers (req, res, next) {
    SchemaHelper.getUsersAsync({ cb : function (err) { next(err); } }, res);
}

function getCategoryTypes (req, res, next) {
    SchemaHelper.getCategoryTypesAsync({ cb : function (err) { next(err); } }, res);
}

function getCategories (req, res, next) {
    SchemaHelper.getCategoriesAsync({
        cb : function (err) { next(err); },
        //get all categories by default (more than 1K)
        limitParams : 0
    }, res);
}

function getDrugs (req, res, next) {
    SchemaHelper.getDrugsAsync({
        cb : function (err) { next(err); },
        //get all drugs by default (more than 3K)
        limitParams : 0
    }, res);
}

function getDrugsCountBy (params, cb) {
    Drug.count(params || {}, cb);
}

function getDrugsTotalCount (req, res, next) {
    getDrugsCountBy({}, function (err, count) { 
        res.locals.recordCount = parseInt(count, 10);
        next(err); 
    });
}

function setupPagingFilter (req, res, next) {

    var defaultFilter = {
        
        totalPages :    0,
        page       :    1,

        sortField  :"_id",
        sortDirection:  1,
        
        find       :   "",

        findFields :   [],

        limit      :  200,
        skip       :    0,
        items      :    0,
        totalItems :    0
    },
        filter = {

    },  filterKeys;

    _.extend(filter, defaultFilter);

    filterKeys = _.keys(filter);

    _.extend(filter,
        _.pick(req.body.filter, filterKeys)
    );

    console.log("_____ FILTER BEFORE EXTENDING _____".green, "\n", _.pick(req.body.filter, filterKeys));
    console.log("_____ FILTER AFTER EXTENDING _____".green, "\n", filter);


    function setupFilterFindFields (filter) {
        // Check find fields
        if (filter.findFields.length <= 0) {
            filter.find = {};
            return;
        }

        var findBy = [];

        filter.findFields.forEach(function (findField) {
            var findObj = {},
                findParam;

            if (findField.value == "false" || findField.value == false) {
                findParam = false;
            } else if (findField.value == "true" || findField.value == true) {
                findParam = true;
            } else if (findField.value == "null" || findField.value == null) {
                findParam = null;
            } else {
                findParam = new RegExp(findField.value);
            }

            findObj[findField.name] = findParam;
            // ex. { 'disabled' : false }

            findBy.push(findObj);
        });

        filter.find = { $or: findBy };

    }

    function setupFilterSort (filter) {
        // Sort by
        filter.sort = {};
        if (filter.sortField && filter.sortDirection) {
            filter.sort[filter.sortField] = filter.sortDirection;
        }
    }

    function setupFilterPagingWithItemsCount(filter, totalItems) {

        // Total items #
        filter.totalItems = totalItems;

        // Fix limit
             if (filter.limit <= 0)                filter.limit = defaultFilter.limit;
        else if (filter.limit > filter.totalItems) filter.limit = filter.totalItems;

        if (filter.page <= 0) filter.page  = defaultFilter.page;

        setupFilterSort(filter);

        // Skips certain limit
        filter.skip = defaultFilter.skip;
        if (filter.page > 1) {
            filter.skip = (filter.page-1) * filter.limit;
        }

        // page, page items #
        var itemsStartAtRecord = filter.totalItems - filter.skip;
        if (itemsStartAtRecord < 0) {
            // Skipped too many records, go back to first page
            filter.page  = defaultFilter.page;
            filter.skip  = defaultFilter.skip;
            filter.limit = defaultFilter.limit;
            filter.items = filter.totalItems > filter.limit ? filter.limit : filter.totalItems;
        }
        if (itemsStartAtRecord > filter.limit) {

            if (filter.limit == filter.totalItems)
                filter.items = filter.totalItems;
            else
                filter.items = filter.limit;

        } else {
            filter.items = itemsStartAtRecord;
        }

        // Total pages #
        filter.totalPages = Math.ceil(filter.totalItems / filter.limit);

        console.log((itemsStartAtRecord + "").green, (filter.totalItems - filter.skip + "").green);
    }


    setupFilterFindFields(filter);

    getDrugsCountBy(filter.find || {}, function getDrugsCountResult (err, count) {
        if (err) return sendErrorResponse(err, res);

        setupFilterPagingWithItemsCount(filter, parseInt(count, 10));

        res.locals.pagingFilter = filter;
        console.log("...........................".red, res.locals.pagingFilter);
        next();
    });
}


// Simple route middleware to ensure user is authenticated.
// Use this route middleware on any resource that needs to be protected.  
// If the request is authenticated (typically via a persistent login session), the request will proceed.  
// Otherwise, the user will be redirected to the login page.
function ensureAuthenticated (req, res, next) {
    // req.session.returnTo = req.originalUrl || req.url || "/";
    if (req.isAuthenticated()) return next();
    res.redirect('/login');
}

function requestedWithWrongType (req, res, next) {
    var errorMessage = "Wrong Request Type.";
    req.flash('error', errorMessage + " Page only accepts " + getRequestAcceptedTypes(req));
    res.send("error " + errorMessage);
}

function getRequestAcceptedTypes (req) {
    var acceptedTypes = [];
    req.accepted.forEach(function (type) {
        acceptedTypes.push(type.value);
    });
    return acceptedTypes.join(", ");
}

function sendErrorResponse (err, res, next) {
    var errorMessage = getErrorMessage(err);

    logger.logError(errorMessage);

    if (res) return res.json({
        success : false,
        error   : errorMessage
    });
    return next && next(err);
}

function sendSuccessResponse (params, res, next) {

    if (params && params.json && typeof params.json === "function") {
        // checks if it is Express's response object wrapper,
        // changes the arguments
        next = res;
        res = params;
        params = {};
    }

    var responseData = { success: true };
    _.extend(responseData, params);

    if (res) return res.json(responseData);
    return next && next();
}

function getErrorMessage (err) {
    var errorMessage = "";
    if (_.isArray(err)) {
        err.forEach(function (e) {
            errorMessage += getErrorString(e);
        });
    } else {
        errorMessage += getErrorString(err);
    }
    return errorMessage;
}

function getErrorString (err) {
    if (err && err.toJSON)   return err.toJSON();
    if (err && err.toString) return err.toString();
    if (err)                 return err;
    return "Undefined error";
}

/**
 * Given drug (with parents), generates asynchronous functions
 * for checking that parents are correctly set up, as well as
 * populating ancestors array (categories) for each of drug parents,
 * which gets returned with each asynchronous function call
 * To capture, use something like async.parallel
 */
function checkDrugAndSetAncestorsFromParents (drug, res, next) {

    // console.log("Drug Parents:", drug.parents);

    var asyncCategoryFunctions = [],
        categoryDoesNotBelongToCategoryTypeError = "The category does not belong to the provided category type";

    if (!drug.parents) {
        asyncCategoryFunctions.push(function (cb) { cb("No drug parent categories are found."); });
        return asyncCategoryFunctions;
    }

    drug.parents.forEach(function (parent) {

        // If the categoryType and parent category are set
        if (parent.categoryType != null && parent.categoryType != "" && 
            parent.parent       != null && parent.parent       != ""   ) {

            asyncCategoryFunctions.push(function (callback) {

                parentCategoryId = parent.parent;
                ancestorCategories = [];

                console.log("Drug parent category type ID#:", parent.categoryType);

                SchemaHelper.getCategoryTypesById(parent.categoryType, {
                    cb: function (err, categoryType) {
                        
                        if (err || !categoryType) {
                            return callback(err || "Could not find appropriate category type");
                        }

//                        console.log("parentCategoryId:", parentCategoryId);

                        // If category type does not have this category
                        if (_.indexOf(categoryType.categories, parent.parent) < 0) {

                            SchemaHelper.getCategoriesById(parentCategoryId, {
                                cb: function (err, parentCategory) {
                                    if (err || !parentCategory) {
                                        return sendErrorResponse(err || "Could not find category", res);
                                    }
                                    ancestorCategories = parentCategory.ancestors;
                                    ancestorCategories.push(parentCategoryId);
                                    parent.ancestors = ancestorCategories;
                                    return callback(null, parent);
                                },
                                findOne : true,
                                lean    : true
                            });
                        } else {
                            sendErrorResponse(categoryDoesNotBelongToCategoryTypeError, res);
                            callback(categoryDoesNotBelongToCategoryTypeError)
                        }
                        
                    },
                    findOne : true,
                    lean    : true
                });
            });
        }
    });

    return asyncCategoryFunctions;
}

app.use("/", router);

app.listen(config.PORT, config.IP);


app.use(function (req, res, next) {

    var query = "";

    if (req.getPath() && req.isAuthenticated()) {
         query = "?redirectedFrom=" + req.getPath();
    }

    // 404 error
    if (req.accepts(["html"]))
        return res.redirect("/" + query);
    if (req.accepts(["json"]))
        return sendErrorResponse("Wrong page", res, next);

    return requestedWithWrongType(req, res, next);
});

console.log(('Express server listening on ' + config.IP + ":" + config.PORT).green);