// Passport & Google Login Strategies
var   passport             = require("passport")
    , GoogleStrategy       = require('passport-google-oauth').OAuth2Strategy
    // Configuration File
    , config               = require('../data/config')
    ;

module.exports = function (User) {

    // Passport session setup.
    //   In order to support login sessions,
    //   Passport will serialize and deserialize user instances to and from the session.

    //   To support persistent login sessions, Passport needs to be able to
    //   serialize users into and deserialize users out of the session.
    //   Typically, this will be as simple as storing the user ID when serializing,
    //   and finding the user by ID when deserializing.
    //   However, since this example does not have a database of user records,
    //   the complete Google profile is serialized and deserialized.
    //   ** Actually I use db, ignore comment above
    passport.serializeUser(function (user, done) {
        // Serialize user's email in session
        // console.log( "Serializing user's email", user.emails[0].value, user._json.name, "\n");
        done(null, user.emails[0].value); // user._json.email
    });

    passport.deserializeUser(function (email, done) {

        // console.log("Deserializing user's email", email, "\n");

        var errorMessage = "No user found";

        // Deserialize user email from session, find in database
        User.findOne({ email: email }, function (err, user) {

            if (err) {
                // logger.logError(err || errorMessage, "User_: ", user);
                done(err || errorMessage);
            }
            if (user != null) {
                // console.log("Found user:", user);
                done(null, user);
            } else {
                // logger.logError(err || errorMessage, "User: ", user);
                done(null, false, { message: errorMessage });
            }
        });
    });


    // Use the GoogleStrategy within Passport.
    //   Strategies in passport require a `validate` function, which accept
    //   credentials (in this case, an OpenID identifier and profile), and invoke a
    //   callback with a user object.
    passport.use(new GoogleStrategy({
            //returnURL: 'http://localhost:3000/auth/google/return',
            //realm: 'http://localhost:3000/',
            clientID     : config.passport.google.CLIENT_ID,
            clientSecret : config.passport.google.CLIENT_SECRET,
            callbackURL  : config.passport.google.REDIRECT_URL
        },
        function (accessToken, refreshToken, profile, done) {
            // asynchronous verification, for effect...
            process.nextTick(function () {

                // To keep the example simple, the user's Google profile is returned to
                // represent the logged-in user.  In a typical application, you would want
                // to associate the Google account with a user record in your database,
                // and return that user instead.

                return done(null, profile);
            });
        }
    ));

    return passport;
};
