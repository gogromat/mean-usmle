// Include gulp
var   gulp   = require('gulp')
	, jshint = require('gulp-jshint')
	, concat = require('gulp-concat')
	, uglify = require('gulp-uglify')
	, rename = require('gulp-rename')
	, sass   = require('gulp-sass')
	, cssmin = require('gulp-cssmin')
	;



gulp.task('default', function () {
    gulp.src('public/stylesheets/*.css')
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('dist'));
});