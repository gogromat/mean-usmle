// Load Logging Library
var winston = require('winston')

// Setup winston to log to a file

// winston.info, winston.warn, winston.error

var Logger = {
	getArgumentsArray: function (args) {
		return Array.prototype.slice.call(args);
	},
	unshiftArguments: function (args, value) {
		var argsArray = Logger.getArgumentsArray(args);
		argsArray.unshift(value);
		return argsArray;
	},
	log: function () {
		winston.log.apply(null, Logger.getArgumentsArray(arguments));
	},
	logError: function () {
		Logger.log.apply(null, Logger.unshiftArguments(arguments, "error"));
	},
	logWarning: function () {
		Logger.log.apply(null, Logger.unshiftArguments(arguments, "warn"));
	},
	logInfo: function () {
		Logger.log.apply(null, Logger.unshiftArguments(arguments, "info"));
	},
	dontLogToConsole: function () {
		// would remove console logging
		winston.remove(winston.transports.Console); 
	},
	dontLogToFile: function () {
		// would remove file logging
		winston.remove(winston.transports.File);
	},
	logToFile: function (filename) {
		winston.add(winston.transports.File, { filename: filename });
	}
}

// Default file
Logger.logToFile('./log.log');
module.exports = Logger;