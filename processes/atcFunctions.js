function isATCMatch (word, letter) {
    // Given the word, tries to match it against multiple RegExp of ATC codes
    // RegExp is split by levels: ex. V 01 A A 01 - V01AA01
    var regExpLeveled = [letter, "\\d{1,3}", "[A-Z]", "[A-Z]", "\\d{1,3}"],
        buildRegExp   = "",
        isATCMatch    = false;

        regExpLeveled.forEach(function (level, index) {
            buildRegExp += level;
            if (new RegExp("^" + buildRegExp + "$").test(word)) isATCMatch = true;
        });
    return isATCMatch;
}