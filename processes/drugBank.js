module.exports = function (spooky, params) {
    
    var data = {},
        drug = params.data.item.name,
        alternativeAddress = params.data.alternativeAddress,
        drugBankAddress = alternativeAddress || 
                          "http://www.drugbank.ca/search?query=" + drug + "&search_type=drugs";
    
    spooky.start();
    spooky.thenOpen(drugBankAddress);
    spooky.then(function () {
        // click on first drug in a list
        this.click('a[href*="/drugs/"]:nth-child(1)');
    });
    spooky.then(function () {

        var keys = [

            // Identification
            {name: "name",              text:  'Name',              object: "identification", callback: "cleanHtml" },
            {name: "accessionNumber",   text:  'Accession Number',  object: "identification", callback: "cleanHtml" },
            {name: "type",              text:  'Type',              object: "identification", callback: "cleanHtml" },
            {name: "groups",            text:  'Groups',            object: "identification", callback: "cleanHtml" },
            {name: "description",       text:  'Description',       object: "identification", callback: "cleanHtml" },
            
            // Taxonomy
            {name: "kingdom",           text:  'Kingdom',           object: "taxonomy" },
            {name: "superclass",        text:  'Superclass',        object: "taxonomy" },
            {name: "class",             text:  'Class',             object: "taxonomy" },
            {name: "subclass",          text:  'Subclass',          object: "taxonomy" },                    
            {name: "directParent",      text:  'Direct parent',     object: "taxonomy" },                    
            {name: "alternativeParents",text:  'Alternative parents',               object: "taxonomy" },
            {name: "substituents",      text:  'Substituents',                      object: "taxonomy" },                    
            {name: "classificationDescription", text:  'Classification description',object: "taxonomy" },

            // Pharmocology
            {name: "indication",        text:  'Indication',        object: "pharmacology", callback: "cleanHtml"  },
            {name: "pharmacodynamics",  text:  'Pharmacodynamics',  object: "pharmacology", callback: "cleanHtml"  },
            {name: "mechanism",         text:  'Mechanism',         object: "pharmacology", callback: "cleanHtml"  },
            {name: "absorption",        text:  'Absorption',        object: "pharmacology", callback: "cleanHtml" },
            {name: "volumeOfDistribution",  
                                        text: 'Volume of distribution',   
                                                                    object: "pharmacology", callback: "cleanHtml"},
            {name: "proteinBinding",    text:  'Protein binding',   object: "pharmacology", callback: "cleanHtml" },
            {name: "metabolism",        text:  'Metabolism',        object: "pharmacology", callback: "cleanHtml" },
            {name: "halfLife",          text:  'Half life',         object: "pharmacology", callback: "cleanHtml"  },
            {name: "clearance",         text:  'Clearance',         object: "pharmacology", callback: "cleanHtml" },
            {name: "toxicity",          text:  'Toxicity',          object: "pharmacology", callback: "cleanHtml"  },
            {name: "affectedOrganisms", text:  'Affected organisms',object: "pharmacology", callback: "cleanHtml" },

        ],
        drugInfo = ".drug-table";
        
        // If there is a drug description
        if (this.exists(drugInfo)) {
            
            data = this.evaluate(function (drugInfo, keys) {
                
                var drugBankData = {},
                    keyValue,
                    cleanHtml = function (obj) {
                        return obj.text();
                    },
                    keyResult;
                
                keys.forEach(function (data) {

                    keyValue = $($(drugInfo).find("th:contains('" + data.text + "')")[0]).parent().children("td");
                    
                    if (keyValue) {
                        if (data.callback && data.callback == "cleanHtml") {
                            keyResult = cleanHtml($(keyValue));
                        } else {
                            keyResult = $(keyValue).html();
                        }
                        
                        if (data.object) {
                            if (!drugBankData[data.object]) drugBankData[data.object] = {};
                            drugBankData[data.object][data.name] = keyResult;
                        } else {
                            drugBankData[data.name] = keyResult;
                        }
                    
                    }
                
                });
                
                return drugBankData;
                
            }, drugInfo, keys);
        }
        
        this.emit("done", JSON.stringify(data));
    });
    
    spooky.on("done", function (data) {
        if (params.callback) params.callback(null, data);
    });
    
    spooky.run();
};