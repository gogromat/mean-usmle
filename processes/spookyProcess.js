var Spooky = require('spooky');

var SpookyProcess = function (processFile, params) {
    
    var spooky = new Spooky({
        child: params.child || {
            transport: 'http',
            // transport: 'stdio',
            port: 16001
        },
        casper: params.casper || {
            logLevel: 'debug',
            verbose: true,
            options: {
                //todo: do it outside of phantom for speed
                clientScripts: ['node_modules/jquery/dist/jquery.min.js']
            }
        }
    }, function (err) {

        if (err) {
            console.log("OVER HERE");
            console.log('Failed to initialize SpookyJS');
            var e = new Error('Failed to initialize SpookyJS');
            // e.details = err;
            throw e;
        } else {
            
            // call the process file with spooky
            require(
                ("./" || params.processesFolder) + processFile + ".js"
            )(spooky, params);
        }
    });

    spooky.on('error', function (e, stack) {
        console.error(e);
        if (params && params.callback) {
            params.callback(e);
        }
        if (stack) {
            console.log(stack);
        }
    });
    
    /* remote console.log */
    spooky.on('remote.message', function (msg) { 
        console.log('Remote console: ' + msg); 
    }); 
    
    /*
    // Uncomment this block to see all of the things Casper has to say.
    // There are a lot.
    // He has opinions.
    */
    spooky.on('console', function (line) {
        console.log(line);
    });
        
    spooky.on('log', function (log) {
        if (log.space === 'remote') {
            console.log(log.message.replace(/ \- .*/, ''));
        }
    });    
}


module.exports = SpookyProcess;