module.exports = function (spooky, params) {
    
    var data = {},
        letter = params.data.letter,
        ATCCodes = {};

    spooky.on("done", function (data) {
        if (params.callback) params.callback(null, data);
    });
    
    function addStep (code) {

        console.log("CLICKED ON THE NEXT LINK IN LIST: [" +  code + "]");
        
        spooky.thenOpen("http://www.whocc.no/atc_ddd_index/?code=" + code + "&showdescription=yes");

        spooky.then(function () {

            console.log("NEW PAGE EXECUTION");

            var params = {
                    content: "#content",
                    linkWithCode: "a[href^='./?code=']"
                },
                newATCCodes;


            try {
                this.page.injectJs('node_modules/jquery/dist/jquery.min.js');
            } catch (e) {
                console.log(e);
            }

            if (this.exists(params.content)) {
                
                newATCCodes = this.evaluate(function (params) {
                    // V - level 0
                    // V01 - level 1
                    // V01A - level 2
                    // V01AA - level 3
                    // V01AA01 - Level 4

                    var atcData = {},
                        links = $(params.content).find(params.linkWithCode);
                    
                    $.each(links, function (i, link) {
                        
                        var isCode = /.*code=([A-Z0-9]{1,7})&?.*/.exec($(link).attr('href')),
                            code   = (isCode.length >= 1 ? isCode[1] : ""); 
                        
                        if (code != "" && !atcData[code]) {
                            atcData[code] = {
                                text: $(link).text(),
                                        // first level ("V") and drugs ("V01AA01") were visited already
                                visited: (isCode[1].length == 1 || isCode[1].length == 7 ? true : false),
                                href: $(link).attr('href')
                            };
                        }
                    });

                    return atcData;

                }, params);
                
                this.emit("renewATCCodes", newATCCodes);
            
            } else {
                this.emit("renewATCCodes");
            }

            // this.emit("done", JSON.stringify(newATCCodes));
        });
    }

    spooky.on("renewATCCodes", function (data) {

        var self = this;

        console.log("\n\r NEW DATA OBTAINED \n\r");
        // console.log(JSON.stringify(data));

        var newCodes = {};
        
        // copy over the codes
        for (var i in data) {
            if (!ATCCodes[i]) {
                ATCCodes[i] = newCodes[i] = data[i];
            } else {
                console.log("MARK VISITED CODE [" + i + "]");
                // the codes were already there, but we went deeper into some of them
                ATCCodes[i].visited = true;
            }
        }

        // iterate over newly acquired links
        for (var j in newCodes) {
            if (!ATCCodes[j].visited) {

                self.emit("shits", "govno");

                addStep(j);

                console.log("BREAKING [" + j + "]");
                break;
                // while (!ATCCodes[j].visited) {
                    // setTimeout(function () {
                        // console.log("wait [" + j + "]");
                    // }, 2000);
                // }
            }
        }        
        // console.log("DATA IS:"); console.log(JSON.stringify(ATCCodes));

    });

    spooky.start();
    addStep(letter, spooky);
    spooky.run();
};