module.exports = function (spooky, params) {
    
    var data = {},
        drug = params.data.item.name,
        alternativeAddress = params.data.alternativeAddress,
        wikipediaAddress = alternativeAddress || 
                          "http://en.wikipedia.org/wiki/" + drug;
    
    spooky.start();
    spooky.thenOpen(wikipediaAddress);
    
    spooky.then([{
        "locals": {
            "drugName": drug,
            "link"    : { "name": "Wikipedia", "url": wikipediaAddress },
            "drugInfo": ".infobox",
            "keys": [
                // Clinical
                {name: "brandName",         text:  'Trade name', object: "clinical" }, 
                
                // Pharmacokinetics
                {name: "bioavailability",   text:  'Bioavailability',                               object: "pharmacokinetics" },
                {name: "metabolism",        text:  'metabolism',            callback: "cleanHtml",  object: "pharmacokinetics" }, 
                {name: "halfLife",          text:  'half-life',                                     object: "pharmacokinetics" }, 
                {name: "excretion",         text:  'Excretion',             callback: "cleanHtml",  object: "pharmacokinetics" },
                
                // Identifiers
                {name: "CASnumber",         text:  'CAS',                   callback: "cleanHtml",  object: "identifiers"},
                {name: "ATCcode",           text:  'Antomical Therapeutic', callback: "cleanHtml",  object: "identifiers" },
                {name: "PubChem",           text:  'PubChem',               callback: "cleanHtml",  object: "identifiers" },
                {name: "DrugBank",          text:  'DrugBank',              callback: "cleanHtml",  object: "identifiers" },
                {name: "ChemSpider",        text:  'ChemSpider',            callback: "cleanHtml",  object: "identifiers" },
                {name: "UNII",              text:  'Unique Ingredient Identifier',  callback: "cleanHtml", object: "identifiers" },
                {name: "KEGG",              text:  'KEGG',                  callback: "cleanHtml",  object: "identifiers" },
                {name: "ChEBI",             text:  'ChEBI',                 callback: "cleanHtml",  object: "identifiers" },
                {name: "ChEMBL",            text:  'ChEMBL',                callback: "cleanHtml",  object: "identifiers" },
                
                // Chemical
                {name: "chemicalFormula",   text:  'Chemical formula',      callback: "cleanHtml",  object: "chemical"     },
                {name: "molecularMass",     text:  'Molecular mass'     ,                           object: "chemical"     },
                {name: "SMILES",            text:  'Simplified molecular input', callback: "cleanHtml", object: "chemical"      },
                {name: "InChI",             text:  'International Chemical Identifier', callback: "cleanHtml", object: "chemical" },
                
                // Physical
                {name: "density",           text:  'Density',                                       object: "physical"      },
                {name: "meltingPoint",      text:  'Melting point',     callback: "cleanHtml",      object: "physical"      },
                {name: "solubility",        text:  'Solubility',        callback: "cleanHtml",      object: "physical"      },
                
                // image
                {name: "image",             element: "image"}
            ]
        }
    }, function () {
        
        // If there is a drug description
        if (this.exists(locals.drugInfo)) {

            data = this.evaluate(function (locals) {
                
                var wikiData = {},
                    cleanHtml = function (obj) {
                        return obj.text();
                    },
                    getImage = function (imageName) {
                        return $(locals.drugInfo).find("img[alt*='" + imageName + "']");
                    },
                    name = locals.drugName,
                    upperCased = (name).toUpperCase(),
                    withoutFirstLetter = name.substr(1, name.length),
                    withoutFirstLetterUpToSpace = withoutFirstLetter.substr(0, withoutFirstLetter.indexOf(' '));

                locals.keys.forEach(function (data) {
                    
                    var element   = "link",
                        keyValue  = "",
                        keyResult = "";

                    if (!data.element) data.element = "link";

                    if (data.element == "image") {

                        if (name.length) {
                            
                            keyValue = getImage(name);

                            if (!keyValue.length && upperCased.length)
                                keyValue = getImage(upperCased);
                            
                            if (!keyValue.length && withoutFirstLetter.length)
                                keyValue = getImage(withoutFirstLetter);
                            
                            if (!keyValue.length && withoutFirstLetterUpToSpace.length)
                                keyValue = getImage(withoutFirstLetterUpToSpace);
                            
                            if (keyValue.length) {
                                keyResult = keyValue.attr("src");
                            }
                        }


                    }
                    if (data.element == "link") {
                        keyValue = $(locals.drugInfo).find("a[title*='" + data.text + "']");  
                    }

                    if (keyValue) {
                        
                        if (!keyResult) {                        
                            if (data.callback && data.callback == "cleanHtml") {
                                keyResult = cleanHtml($(keyValue.parent().parent().children()[1]));
                            } else {
                                keyResult = $(keyValue.parent().parent().children()[1]).html();
                            }
                        }
                        
                        if (data.object) {
                            if (!wikiData[data.object]) wikiData[data.object] = {};
                            wikiData[data.object][data.name] = keyResult;
                        } else {
                            wikiData[data.name] = keyResult;
                        }
                    }
                });

                wikiData.link = locals.link;
                
                return wikiData;
                
            }, locals);

        }

        this.emit("done", JSON.stringify(data));
    }]);
    
    spooky.on("done", function (data) {
        if (params.callback) params.callback(null, data);
    });
    
    spooky.run();
};