// underscore / Lodash library
angular
    .module('lodash', [])
    .factory('_', ['$window', function ($window) {
        return $window._;
    }]);

// Kris Kowal's Q library - for jsTree module
angular
    .module('q', [])
    .factory('q', ['$window', function ($window) {
        return $window.Q;
    }]);

// Jade client-size build template functions (clientJade library)
angular
    .module('jade', [])
    .factory('jade', ['$window', function ($window) {
        return $window.jade;
    }]);

// POJO JsTree library
angular
    .module('JSTree', ["q", "jade"])
    .factory('JSTree', ['$window', function ($window) {
        return $window.JSTree;
    }]);

//'ui.tree',
var UDApp = angular.module('UDModule', [

    'bindOnce', 'once', 'pasvaz.bindonce', 'delayedModel',

    'angularBootstrapNavTree', 'JSTree', 'treeControl',

    'angularFileUpload',
    'textAngular',
    'flash',
    'confirmBox',
    'ngSanitize',
    'lodash',
    'textFilters',
    'ngRoute',
    'ui.bootstrap'
]);

UDApp.config(function ($routeProvider, $locationProvider) {

//    console.log($routeProvider, $locationProvider, "Location Query");

    $routeProvider

        // Index page
        .when("/", {
            templateUrl: "/index",
            controller: "IndexController"
        })

        // Auth actions
        .when("/login",       { templateUrl: "/login" })
        // todo fix
        .when("/logout",      { templateUrl: "/logout"})
        .when("/auth/google", { redirectTo : "/auth/google" })


        // API actions
        // USERS
        .when("/users", {
            templateUrl: "/api/users",
            controller: "UserController",
            resolve: {
                isLoggedIn: ViewController.checkLoggedIn
            }
        })

        // CATEGORIES
        .when("/categories", {
            templateUrl: "api/categories",
            controller: "CategoryController",
            resolve: {
                isLoggedIn: ViewController.checkLoggedIn
            }
        })

        // CATEGORY TYPES
        .when("/categoryTypes", {
            templateUrl: "api/categoryTypes",
            controller: "CategoryTypeController",
            resolve: {
                isLoggedIn: ViewController.checkLoggedIn
            }
        })

        // DRUG
        .when("/drugs/new", {
            templateUrl: "api/drugs/new",
            controller: "DrugController",
            resolve: {
                isLoggedIn: ViewController.checkLoggedIn
            }
        })
        .when("/drugs/:drugId", {
            templateUrl: function (params) {
                return "api/drugs/" + params.drugId;
            },
            controller: "DrugController",
            resolve: {
                isLoggedIn: ViewController.checkLoggedIn
            }
        })

        // DRUGS
        .when("/drugs", {
            templateUrl: "api/drugs",
            controller: "DrugsController",
            resolve: {
                isLoggedIn: ViewController.checkLoggedIn
            }
        })

        // Default action
        .otherwise({
            redirectTo: "/"
        });

    // Use the HTML5 History API
    $locationProvider.html5Mode(true);
});


UDApp.controller("AppController", function ($rootScope, $location, $routeParams) {

    $rootScope.$on("$routeChangeError", function (event, current, prev) {
        console.log("There was an error");
    });

    $rootScope.$on('$locationChangeStart', function(event, next, current) {

        var redirectingLocation = $location.search().redirectedFrom;

        if (redirectingLocation) {
            delete $location.$$search.redirectedFrom;
            $location.$$compose();
            $location.path(redirectingLocation);
        }

//        console.log(
//            "rootScope changing location",
//            $location.path(),
//            $location.search(),
//            $location.search().redirectedFrom
//        );

        // If next path is same as this one
//        if ($location.path() ==  helperService.getPathFromURL(current)) {
            // console.log("URL:\n",$location.url(), "\nPath:",$location.path(), "\nHash:",$location.hash());
//            pagingService.setFilter($location.search());
//        }

    });
});

var ViewController = UDApp.controller("ViewController", function ($scope, $route, $location) {

    $scope.changeRoute = function () {
        console.log($scope);
    };

});

ViewController.checkLoggedIn = function ($q, $http, $location, flashService) {
    var defer = $q.defer();

    function sendError() {
        flashService.flash("error", "You are not logged in.");
        defer.reject({ isAuthenicated: false });
    }

    $http({
        url     : '/isLoggedIn',
        method  : "GET"
    }).success(function (data, status, headers, config) {
        if (data.success == true && data.isLoggedIn == true) {
            defer.resolve({ isAuthenicated: true });
        } else {
            sendError();
        }
    }).error(function (data, status, headers, config) {
//        $location.();
        console.log("Checking if logged in - There was an error!", data);
        sendError();
    });

    return defer.promise;
};