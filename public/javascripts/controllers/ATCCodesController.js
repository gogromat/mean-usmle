function ATCCodesController ($scope, $http, _, appService, flashService) {
    
    $scope.codes = [
        { code: "A", name:    "Alimentary tract and metabolism", selected: false, visited: false },
        { code: "B", name:    "Blood and blood forming organs", selected: false, visited: false },
        { code: "C", name:    "Cardiovascular system", selected: false, visited: false },
        { code: "D", name:    "Dermatologicals", selected: false, visited: false },
        { code: "G", name:    "Genito-urinary system and sex hormones", selected: false, visited: false },
        { code: "H", name:    "Systemic hormonal preparations, excluding sex hormones and insulins", selected: false, visited: false },
        { code: "J", name:    "Antiinfectives for systemic use", selected: false, visited: false },
        { code: "L", name:    "Antineoplastic and immunomodulating agents", selected: false, visited: false },
        { code: "M", name:    "Musculo-skeletal system", selected: false, visited: false },
        { code: "N", name:    "Nervous system", selected: false, visited: false },
        { code: "P", name:    "Antiparasitic products, insecticides and repellents", selected: false, visited: false },
        { code: "R", name:    "Respiratory system", selected: false, visited: false },
        { code: "S", name:    "Sensory organs", selected: false, visited: false },
        { code: "V", name:    "Various", selected: false, visited: false }    
    ];

    $scope.fetchAndSetATCCodes = function (atcCategoryType) {

        var errorMessage   = "Could not fetch and store ATC codes",
            successMessage = "Successfully fetched and stored ATC codes",
            codesToFetch = _.filter($scope.codes, { selected: true });

        function isError (data, status, headers, config) {
            flashService.flash('error', data.error || errorMessage);
            console.log("Error:", data);
        }

        console.log(codesToFetch);

        $http({
            url: '/api/scrape/atc',
            method: 'POST',
            data: {
                categoryType: atcCategoryType,
                codes: codesToFetch
            }
        }).success(function (data, status, headers, config) {
            if (data.success && data.success == true) {
                flashService.flash(successMessage);
            } else {
                isError(data, status, headers, config);
            }
        }).error(isError);
    };

}

ATCCodesController.$inject = ['$scope', '$http', '_','appService', 'flashService'];