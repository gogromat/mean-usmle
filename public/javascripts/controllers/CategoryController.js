function CategoryController ($scope, $http, appService, flashService, categoryService, categoryTypeService, $location, dbService) {

    // DB Categories are part of categoryType 
    $scope.categoryTypes = [];

    // DB Categories
    $scope.categories = [];

    // DB Categories reorganized    
    $scope.catsWithNew = [];
    
    // Currently selected DB category
    $scope.currentCategoryType;
    $scope.currentCategory;

    $scope.newCategories = [{}];

    $scope.addNewCategory = function () {
        $scope.newCategories.push({});
    };

    $scope.saveNewCategories = function () {

        var errorMessage   = "Could not save new categories",
            successMessage = "Successfully saved new categories";

        dbService.addCategory(null, function (result) {
            if (result.error) {
                flashService.error(errorMessage);
                return;
            }
            flashService.flash(successMessage);
            $scope.newCategories = [{}];
            // todo make it better - save the data instead of re-fetching
            fetchCategoriesAndCategoryTypes();
        }, {
            category: $scope.currentCategory || "",
            categoryType: $scope.currentCategoryType,
            categoryNames: _.pluck($scope.newCategories, 'name')
        });
    };

    // changes current category type, populates proper categories
    $scope.changeCategoryType = function (categoryType) {
        var nextCategoryType,
            categoryTypeCategories;
        $scope.categoryTypes.forEach(function (ct) {
            if (ct.name == categoryType.name) {
                nextCategoryType = ct;
            }
        });
        $scope.currentCategoryType = nextCategoryType;
        categoryTypeCategories =  categoryService.getCategoriesMatchingCategoryTypeCategories($scope.currentCategoryType);
        $scope.currentCategory = null;
        appService.setCategories(categoryTypeCategories);
        $scope.$broadcast("setCategories");
    };

    $scope.selectTreeCategory = function (branch, triggeredDeleteAction, categoryIsADrug) {

        var errorMessage         = "Could not delete category",
            confirmMessage       = "This category contains subcategories. Want to proceed deleting?",
            successMessage       = "Successfully deleted category",
            category             = appService.getCategoryById(branch._id);

        $scope.currentCategory = category;

        if (triggeredDeleteAction) {

            if (!branch.children.length || (branch.children.length > 0 && window.confirm(confirmMessage)) ) {

                dbService.deleteCategory(branch, function (result) {
                    if (result.error) {
                        return flashService.error(errorMessage);
                    }
                    flashService.flash(successMessage);
                    // TODO: make it better, but for now refetch the data
                    fetchCategoriesAndCategoryTypes();
                });
            }

        } else if (categoryIsADrug) {
            appService.setDrugCategory(categoryIsADrug, category);
        }
    };

    function removeCategoryReference (category) {
        // remove category from categories
        $scope.categories.splice( $scope.categories.indexOf(category), 1);

        // remove category from categoryTypes
        var currentCategoryTypeIndex = $scope.categoryTypes.indexOf($scope.currentCategoryType),
            currentCategoryTypeCategories = $scope.categoryTypes[currentCategoryTypeIndex].categories;
        currentCategoryTypeCategories.splice( currentCategoryTypeCategories.indexOf(category), 1);
    }

    $scope.changeCurrentCategoryName = function () {

        var newName = $scope.currentCategory.name,
            errorMessage   = "Could not change category's name",
            successMessage = "Successfully changed category's name";

        if (newName) {

            dbService.updateCategory($scope.currentCategory, function (result) {
                if (result.error) {
                    return flashService.error(data.error || errorMessage);
                }
                flashService.flash(successMessage);
                //TODO: set the data, don't refetch it
                fetchCategoriesAndCategoryTypes();
                $scope.changeCategoryType($scope.currentCategoryType);
            });
        }
    };

    $scope.setDrugs = function (drugs) {
        console.log("CategoryController::setDrugs()");
        appService.setDrugs(drugs);
    };

    $scope.$on("setCategories", function () {
        $scope.catsWithNew = appService.getCategoriesTree([{label: "New top category", visible: true}]);
    });

    flashService.flash();

    function fetchCategoriesAndCategoryTypes () {
        dbService.getCategories(null, function (results) {
            if (results.error) {
                flashService.flash('error', "Error obtaining category type & category lists from the app.");
                $scope.categories = [];
                $scope.categoryTypes = [];
                return;
            }
            $scope.categories = results.data.categories;
            $scope.categoryTypes = results.data.categoryTypes;

            categoryService.setCategories($scope.categories);
            categoryTypeService.setCategoryTypes($scope.categoryTypes);
            $scope.changeCategoryType($scope.categoryTypes[0]);
        });
    }

    flashService.flash();
    fetchCategoriesAndCategoryTypes();
}

CategoryController.$inject = [
    '$scope', '$http', 
    'appService', 'flashService', 
    'categoryService', 'categoryTypeService', '$location', 'dbService'
];