function CategoryTypeController ($scope, $http, appService, flashService, dbService, _) {

    $scope.categoryTypes = [];
    $scope.atcCategoryType = null;

    $scope.newCategoryType = {};


    $scope.addCategoryType = function () {

        var errorMessage   = "Could not save new category type",
            successMessage = "Successfully added new category type";

        if (!_.isEmpty($scope.newCategoryType)) {

            dbService.addCategoryType($scope.newCategoryType, function (result) {
                if (result.error) {
                    return flashService.flash('error', result.error || errorMessage);
                }
                $scope.newCategoryType = {};
                $scope.categoryTypes.push(result.data.categoryType);
                flashService.flash(successMessage);

                appService.fetchCategories(true); // ???
            });
        }
    };

    $scope.changeCategoryType = function (categoryType) {

        var errorMessage   = "Could not change category type",
            successMessage = "Successfully changed category type";

        dbService.updateCategoryType(categoryType, function (result) {
            if (result.error) {
                return flashService.flash('error', result.error || errorMessage);
            }
            flashService.flash(successMessage);
        });
    };

    $scope.deleteCategoryType = function (categoryType) {
        
        var errorMessage   = "Could not delete category type",
            successMessage = "Successfully deleted category type";

        dbService.deleteCategoryType(categoryType, function (result) {
            if (result.error) {
                return flashService.flash('error', result.error || errorMessage);
            }
            $scope.categoryTypes.splice( $scope.categoryTypes.indexOf(categoryType), 1);
            flashService.flash(successMessage);
            appService.fetchCategories(true);
        });

    };

    function fetchCategoryTypes() {
        dbService.getCategoryTypes(null, function (results) {
            if (results.error) {
                flashService.flash('error', "Error obtaining category type list from the app.");
                return $scope.categoryTypes = [];
            }
            $scope.categoryTypes = results.data.categoryTypes;
        });
    }

    fetchCategoryTypes();
}

CategoryTypeController.$inject = ['$scope', '$http', 'appService', 'flashService', 'dbService', '_'];