function DrugController ($scope, appService, flashService, drugService, categoryTypeService, categoryService, dbService,
                         scrapeService, _, $location, $routeParams, $q) {

    $scope.drug = {};
    $scope.categories = [];
    $scope.categoryTypes = [];
    $scope.drugParents = [];

    $scope.loadingImage = new Image();
    _.extend($scope.loadingImage, {
        src: "http://localhost:3000/images/loading.gif", width:  50, height: 50
    });


    // For now just remove image from drug, do not delete it from server
    $scope.removeImage = function () {
        $scope.drug.image.text = "";
    };

    // LINKS

    $scope.addLinkToDrug = function (link, updatingOld) {

        var successMessage, errorMessage;

        if (updatingOld) {
            successMessage = "Successfully updated drug link(s)";
            errorMessage   = "Could not update drug link(s)";
        } else {
            successMessage = "Successfully added a link to drug";
            errorMessage   = "Could not add a link to drug";
            // add the new link to the drug
            $scope.drug.links.push(link);
        }
        
        $scope.updateDrug(function (results) {
            if (results.error) {
                if (!updatingOld) {
                    // remove the just added link
                    $scope.drug.links.pop();
                }
                flashService.error(errorMessage);
            } else {                
                if (!updatingOld) {
                    link = { name: "", url : "" };
                }
                flashService.flash(successMessage);
            }
        }, rebuildTree = false);
    };

    $scope.updateDrugLinks = function () {
        $scope.addLinkToDrug(null, true);
    };

    $scope.deleteDrugLink = function (link) {
        var oldLinks = angular.copy($scope.drug.links);

        // remove deleted link
        $scope.drug.links.splice( $scope.drug.links.indexOf(link), 1);

        var successMessage = "Successfully deleted a drug link",
            errorMessage   = "Could not delete a link from a drug";

        function ifErrorCallback() {
            flashService.error(errorMessage);
            // put link back
            $scope.drug.links = oldLinks;
        }

        $scope.updateDrug(function (results) {
            if (results.error) {
                ifErrorCallback();
            } else {
                flashService.flash(successMessage);
            }
        });

    };


    // ADD, UPDATE, DELETE

    $scope.addDrug = function () {
        dbService.addDrug($scope.drug, function (results) {
            if (results.error) {
                flashService.error(results.data.error || "Could not add new drug");
                console.log(results.data);
            } else {                
                flashService.flash("Successfully saved drug");
                // redirect to the new drug page
                $location.path('/api/drugs/' + results.data.drug._id);
            }
        });
    };
    
    $scope.updateDrug = function (callback, rebuildTree) {

        if (!callback) {
            // default Callback function
            callback = function(results) {
                console.log("4");
                if (results.error) {
                    console.log("Error:" + results.data);
                    flashService.error(results.data);
                } else {
                    flashService.flash("Successfully updated");
                }
            };
        }

        dbService.updateDrug($scope.drug, function (results) {
            if (!results.error) {
                // Always copy data's drug
                _.extend($scope.drug, results.data.drug);

                if (rebuildTree == true || rebuildTree == undefined) {
                    setupLocalDrugParentsAndBuildCategoriesTree();
                }
            }
            callback(results);
        });
    };

    $scope.deleteDrug = function () {
        dbService.deleteDrug($scope.drug, function (results) {
            if (results.error) {
                console.log("Error: ", err);
                return flashService.error("Error deleting drug from an app.");
            }
            flashService.flash("Successfully deleted " +
            $scope.drug.name + " from an application.");

            // TODO: fix
            $location.path("/api/drugs");

        });
    };

    $scope.setCurrentCategoryType = function (categoryType) {
        categoryTypeService.setCurrentCategoryType(categoryType);
    };

    $scope.getCategoryTypeById = function (categoryTypeId) {
        return categoryTypeService.getCategoryTypeById(categoryTypeId);
    };


    $scope.getCategoryById = function (categoryId) {
        return categoryService.getCategoryById(categoryId);
    };


    // EVENTS
    $scope.$on("categoryTypeService.setCurrentCategoryType", function () {
        $scope.currentCategoryType = categoryTypeService.getCurrentCategoryType();
    });
    

    // TREE
    $scope.selectTreeCategory = function (branch) {

        var selectedCategory   = categoryService.getCategoryById(branch._id),
            selectedCategoryId = selectedCategory._id;

        function setParentCategory (parent) {
            if (parent.categoryType == $scope.currentCategoryType._id) {
                parent.parent = selectedCategoryId;
            }
        }

        $scope.drug.parents.forEach(setParentCategory);
        $scope.drugParents.forEach(setParentCategory);
    };

    $scope.addImageFileForDrug = function (item, $files, $event) {
        dbService.addImageFileForItem(item, $files, $event);
    };

    // For displaying scrape addresses on view
    $scope.scrapeService = scrapeService;

    $scope.scrapeWebsiteForDrug = function (item, address, alternativeAddress) {
        var errorMessage   = "There was an error fetching results.",
            successMessage = "Successfully scraped " + address + " data.";

        scrapeService.scrapeWebsiteForItem({
            item: item, 
            address: address, 
            alternativeAddress: alternativeAddress,
            showJsonHuman: true
        }, function (results) {
            if (results.error) {
                console.log(results.error);
                flashService.error(errorMessage);
            } else {
                
                // Add image
                if (item.data[address].image) {
                    $scope.addImageFileForDrug(item, null);
                }
                // Add link
                if (item.data[address].link) {
                    $scope.addLinkToDrug(item.data[address].link);
                }

                flashService.flash('success', successMessage);
            }
        });
    };

    function fetchData() {
        var promises = [];
        promises.push(fetchCategoriesAndCategoryTypes());
        promises.push(fetchDrugById($routeParams.drugId));

        $q.all(promises)
            .then(function drugsCategoriesAndCategoryTypesSetCallback () {
                $scope.setCurrentCategoryType($scope.categoryTypes[0]);
                setupLocalDrugParentsAndBuildCategoriesTree();
            });
    }

    function fetchCategoriesAndCategoryTypes () {
        return dbService.getCategories(null, function (results) {
            if (results.error) {
                flashService.error("Error obtaining category type & category lists from the app.");
                $scope.categories = [];
                $scope.categoryTypes = [];
                return;
            }
            $scope.categories = results.data.categories;
            $scope.categoryTypes = results.data.categoryTypes;

            categoryService.setCategories($scope.categories);
            categoryTypeService.setCategoryTypes($scope.categoryTypes);
        });
    }

    function fetchDrugById (drugId) {

        if (!drugId) {
            return;
        }

        return dbService.getDrug("/" + drugId, function drugCallback (result) {

            if (result.error) {
                return flashService.error("There was an error fetching drug", result.error);
            }

            $scope.drug = result.data.items;

            console.log("Fetched drug:", $scope.drug);

            drugService.setDrugDefaultFields($scope.drug);

        });
    }

    function setupLocalDrugParentsAndBuildCategoriesTree () {
        // Rebuild drug parents and category tree
        setupDefaultEmptyDrugParentsForAllCategoryTypes();
        setupLocalDrugParentsForCategoriesTree();
        buildCategoriesTreeForEachOfLocalDrugParents();
    }

    function setupDefaultEmptyDrugParentsForAllCategoryTypes () {
        var allCategoryTypes   = categoryTypeService.getCategoryTypes(),
            allCategoryTypeIds = _.pluck(allCategoryTypes, "_id");
        $scope.drug.parents = drugService.setDefaultParentObjectsByCategoryTypeIds(
            $scope.drug.parents, allCategoryTypeIds
        );
    }

    function setupLocalDrugParentsForCategoriesTree () {
        $scope.drugParents = [];
        // sets the default parent categories
        $scope.drug.parents.forEach(function addParentToDrugParents (drugParent) {
            $scope.drugParents.push({
                parent       : drugParent.parent,
                categoryType : drugParent.categoryType,
                ancestors    : drugParent.ancestors
            });
        });
    }

    function buildCategoriesTreeForEachOfLocalDrugParents () {

        var allCategoryTypes   = categoryTypeService.getCategoryTypes(),
            categoryTypeCategories,
            categoriesTree;

        // Rebuild drug parents categories tree
        allCategoryTypes.forEach(function (ct) {

            categoryTypeCategories = categoryService.getCategoriesMatchingCategoryTypeCategories(ct);

            // returns different result
            categoriesTree = appService.buildTree(categoryTypeCategories);

            $scope.drugParents.forEach(function (parent) {
                if (parent.categoryType == ct._id) {
                    parent.categories = categoriesTree;
                }
            });
        });
    }


    flashService.flash();
    fetchData();
}

DrugController.$inject = [
    '$scope', 'appService', 'flashService',
    'drugService', 'categoryTypeService', 'categoryService', 'dbService', 'scrapeService',
    '_', '$location', '$routeParams', '$q'
];