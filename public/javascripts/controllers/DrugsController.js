function DrugsController (
    $scope, 
    flashService, drugService, dbService, scrapeService, pagingService, helperService,
    _, $location) {

    $scope.drugs = [];

    /* WATCHES */
    $scope.$watch("drugs", function (newDrugValue) {
        if (!_.isEmpty($scope.drugs)) {
            $scope.drugs.forEach(function (drug) {
                drugService.setDrugDefaultFields(drug);
            });
        }
    });

    $scope.disableDrug = function (drug) {
        drug.disabled = !drug.disabled;
        $scope.updateDrug(drug);
    };

    // For displaying scrape addresses on view
    $scope.scrapeService = scrapeService;

    $scope.scrapeWebsiteForDrug = function (item, address, alternativeAddress) {

        scrapeService.scrapeWebsiteForItem({
            item: item, 
            address: address, 
            alternativeAddress: alternativeAddress,
            showJsonHuman: false
        }, function (results) {
            if (results.error) {
                console.log(results.error);
                flashService.flash('error', "There was an error fetching results.");
            } else {
                
                if (item.data[address].link) {
                    // Add link
                    item.links.push(item.data[address].link);
                }                
                if (item.data[address].image) {
                    // Add image and update drug
                    dbService.addImageFileForItem(item, null, null, function (results) {
                        // remove image if there was an error saving it
                        if (results.error) item.image.text = "";
                        $scope.updateDrug(item);
                    });
                } else {
                    // Just update drug
                    $scope.updateDrug(item);
                }
            }
        });
    };

    $scope.updateDrug = function (item) {
        delete item.__v;
        dbService.updateDrug(item, function (results) {
            if (results.error) {
                flashService.flash("error", "Could not update " + item.name);
                console.log("DrugsController:Error", item, results);
            } else {
                flashService.flash("success", "Successfully updated " + item.name);
            }
        });
    };



    // PAGER

    $scope.pager = pagingService.pager;
    pagingService.setSortFields(dbService.getSortableFields("drug"));
    pagingService.setFindFields(dbService.getSearchableFields("drug"));

    // On location change fetch new drugs
    $scope.$on('$locationChangeStart', function(event, next, current) {
        // If next path is same as this one
        if ($location.path() ==  helperService.getPathFromURL(current)) {
            console.log("i prevent stuff");
            event.preventDefault();
            // console.log("URL:\n",$location.url(), "\nPath:",$location.path(), "\nHash:",$location.hash());
            pagingService.setFilter($location.search());
        }
    });

    $scope.$on("pagingService:pager:filter:update", function () {
        fetchDrugs();
    });


    function fetchDrugs() {
        pagingService.get("drugs", function getDrugsCallback (results) {
            if (results.error) {
                $scope.drugs = [];
                flashService.flash("error", "There was an error getting drugs");
            } else {
                $scope.drugs = results.data.items;
            }
        });
    }

    fetchDrugs();
    flashService.flash();
}

DrugsController.$inject = [
    '$scope', 
    'flashService', 'drugService', 'dbService', 'scrapeService', 'pagingService', 'helperService',
    '_', '$location'
];