function IndexController ($scope, appService, flashService, categoryService, categoryTypeService, drugService,
                          helperService, _, dbService, $q, JSTree) {

    $scope.params = {
        TREE_OBJECT: ".tree",
        MODES: {
            STUDY: "study",
            DISPLAY: "display"
        }
    };

    $scope.filter = "";
    $scope.categoryTypes = [];
    $scope.categories    = [];
    $scope.drugs         = [];
    $scope.treeData      = [];
    $scope.mode = $scope.params.MODES.DISPLAY;
    $scope.currentCategoryType;
    $scope.highlightDirectMatches;
    $scope.drugDisplayProperties = [
        { name: "name",         show: true  },
        { name: "brandName",    show: false },
        { name: "mechanism",    show: true  },
        { name: "use",          show: true  },
        { name: "pharmacology", show: true  },
        { name: "sideEffects",  show: true  },
        { name: "other",        show: true  },
        { name: "links",        show: false }
    ];

    // js categories/drugs Tree
    var jsTree = new JSTree();
        jsTree.addObject($scope.params.TREE_OBJECT);



    // changes current category type, populates proper categories
    $scope.changeCategoryType = function (categoryType) {
        var nextCategoryType,
            categoryTypeCategories;

        categoryTypeService.getCategoryTypes().forEach(function (ct) {
            if (ct.name == categoryType.name) {
                nextCategoryType = ct;
            }
        });

        $scope.currentCategoryType = nextCategoryType;

        rebuildTreeData();
    };

    $scope.clearSearch = function () {
        $scope.filter = "";
    };

    $scope.changeMode = function (modeVal) {
        $scope.mode = modeVal;
    };

    $scope.toggleSetting = function (setting) {
        setting.show = !setting.show;
    };

    /* WATCHES */
    $scope.$watch("highlightDirectMatches", function (newValue, oldValue) {
       if (newValue != oldValue) {
           jsTree
           .setParameters({ highlightDirectMatches: newValue })
           .renderTree();
       }
    });

    $scope.$watch("drugDisplayProperties", function changeDrugDisplayProperties (newValues, oldValues) {
        if ($scope.mode == $scope.params.MODES.STUDY) {
            var changedValues = [];
            for (var i = 0, length = newValues.length; i < length; i++) {
                if (oldValues[i].show != newValues[i].show) {
                    changedValues.push(newValues[i]);
                }
            }
            if (_.isEmpty(changedValues)) {
                changedValues = newValues;
            }
            $scope.changeDrugDisplayProperties(changedValues);
        }
    }, true);

    $scope.changeDrugDisplayProperties = function (changedValues) {
        if (changedValues.length) {
            jsTree.setDrugDisplayProperties(changedValues);
        }
    };

    // filter tree on filter change
    $scope.$watch("filter", function filterTree (newValue, oldValue) {
        if (oldValue || newValue) {
            //$scope.treeData = appService.filterTree(newValue);
            jsTree
                .filterTree(newValue)
                .renderTree();
        }
    });

    // change structure of drugs on mode change
    $scope.$watch("mode", function changeDisplayMode (newValue, oldValue) {
        if (newValue != oldValue) {
            // $scope.modeDrugs = drugService.changeDrugsByMode(drugService.getDrugs(), newValue, oldValue);
            // console.log("mode changed");
            // rebuildTreeData();
            if (newValue = $scope.params.MODES.STUDY) {
                $scope.changeDrugDisplayProperties($scope.drugDisplayProperties);
            }
        }
    });

    $scope.$watch("treeData", function (newValue, oldValue) {
        if (newValue != oldValue) {
            jsTree
                .addTree(newValue)
                .filterTree($scope.filter)
                .renderTree();
        }
    });

    // Boolean to load modal window
    $scope.setLoadModal = function (item) {
        if (!item.loadModal) item.loadModal = true;
    };


    // rebuild the tree given new categories and drugs. when filter is set filters data as well
    function rebuildTreeData () {

        var categories = categoryService.getCategoriesMatchingCategoryTypeCategories($scope.currentCategoryType),
            drugs = drugService.getDrugsByCategoryType($scope.currentCategoryType);


//        var rebuiltTree = appService.buildTreeWithDrugs(categories, drugs, $scope.currentCategoryType);


        if ($scope.filter) {
            console.log("filter");
            $scope.treeData = appService.filterTree($scope.filter);
        } else {
            console.log("no filter");
            var rebuiltTree = appService.buildTreeWithDrugs(categories, drugs, $scope.currentCategoryType);
            $scope.treeData = rebuiltTree;
            console.log("New Tree:",rebuiltTree);
        }

    }

    $scope.changeRoute = function () {
        console.log("Index changing route", $scope);
    };

    function fetchDrugs() {
        return dbService.obtainDrugs(null, function (results) {

            if (results.error) {
                flashService.error("There was an error getting drugs");
                $scope.drugs = [];
                return;
            }
            $scope.drugs = results.data.items;

            $scope.drugs.forEach(function (drug) {
                drugService.setDrugDefaultFields(drug);
            });
            drugService.setDrugs($scope.drugs);

        }, {
            filter: {
                findFields : [
                    {name: 'disabled', value:  null },
                    {name: 'disabled', value: false }
                ],
                page: 1,
                sortField: "parents.parent",
                sortDirection: 1,
                skip: 0,
                limit: 10000
            }
        });
    }

    function fetchCategoriesAndCategoryTypes () {
        return dbService.getCategories(null, function (results) {
            if (results.error) {
                console.log("Categories - Error");
                flashService.error("Error obtaining category type & category lists from the app.");
                $scope.categories = [];
                $scope.categoryTypes = [];
                return;
            }
            $scope.categories    = results.data.categories;
            $scope.categoryTypes = results.data.categoryTypes;

            categoryService.setCategories($scope.categories);
            categoryTypeService.setCategoryTypes($scope.categoryTypes);
        });
    }

    function fetchData() {
        $q.all([
            fetchDrugs(),
            fetchCategoriesAndCategoryTypes()
        ]).then(function (drugsCategoriesAndCategoryTypes) {
            $scope.changeCategoryType($scope.categoryTypes[0]);
        });
    }

    flashService.flash();
    fetchData();
}

IndexController.$inject = [
    '$scope', 
    'appService', 'flashService', 
    'categoryService', 'categoryTypeService', 'drugService', 'helperService',
    '_', 'dbService', '$q', 'JSTree'];