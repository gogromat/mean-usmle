function UserController ($scope, $http, appService, flashService, helperService, dbService) {

    $scope.users = [];

    $scope.addUserToScope = function () {
        $scope.users.push({ isNew: true, username: "", email: ""});
    };

    $scope.removeUserFromScope = function (user) {
        $scope.users.splice( $scope.users.indexOf(user), 1 );
    };

    $scope.verifyUserEmail = function (user) {
        if (helperService.verifyEmail(user.email)) {
            flashService.error("Invalid email address: " + user.email);
            return false;
        }
        return true;
    };

    $scope.addUser = function (user) {

        if (!$scope.verifyUserEmail(user))
            return false;

        dbService.addUser(user, function (results) {
            if (results.error) {
                return flashService.flash('error', results.data || "Could not save new user");
            }
            user._id = results.data.user._id;
            user.isNew = false;
            flashService.flash('success', 'Successfully saved');
        });
    };

    $scope.updateUser = function (user) {
        if (user.isNew) {
            return $scope.addUser(user);
        }

        if (!$scope.verifyUserEmail(user))
            return false;

        dbService.updateUser(user, function (results) {
            if (results.error) {
                return flashService.flash('error', data || "Could not update user");
            }
            flashService.flash('success', 'Successfully updated');
        });
    };

    $scope.deleteUser = function (user) {
        
        if (user.isNew) {
            $scope.removeUserFromScope(user);
        } else {
            dbService.deleteUser(user, function (results) {
                if (results.error) {
                    flashService.flash('error', results.error || "Error deleting user from an app.");            
                } else {
                    flashService.flash("Successfully deleted " + user.username + " from an application.");                    
                    $scope.removeUserFromScope(user);
                }
            });
        }
    };

    function fetchUsers () {
        dbService.getUsers(null, function (results) {
            if (results.error) {
                flashService.flash('error', results.error || "Error obtaining user list from the app.");
                $scope.users = [];
            } else {
                $scope.users = results.data.users;
            }
        });
    }

    flashService.flash();
    fetchUsers();
}

UserController.$inject = ['$scope', '$http', 'appService', 'flashService', 'helperService', 'dbService'];