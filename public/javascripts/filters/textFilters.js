angular.module('textFilters', [])
.filter('deCamelCaseFilter', function () {
    return function deCamelCase (ccString) {
        var deCamelized = ccString.match(/([A-Z]?[^A-Z]*)/g).slice(0,-1).join(" ");
        return deCamelized[0].toUpperCase() + deCamelized.slice(1);
    };
})
.filter('capitalizeFilter', function() {
    return function(input, scope) {
        if (input) {
            // input = input.toLowerCase();
            return input.substring(0,1).toUpperCase() + input.substring(1);
        }
        return;
    }
});