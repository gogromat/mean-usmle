
jade = (function(exports){
/*!
 * Jade - runtime
 * Copyright(c) 2010 TJ Holowaychuk <tj@vision-media.ca>
 * MIT Licensed
 */

/**
 * Lame Array.isArray() polyfill for now.
 */

if (!Array.isArray) {
  Array.isArray = function(arr){
    return '[object Array]' == Object.prototype.toString.call(arr);
  };
}

/**
 * Lame Object.keys() polyfill for now.
 */

if (!Object.keys) {
  Object.keys = function(obj){
    var arr = [];
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        arr.push(key);
      }
    }
    return arr;
  }
}

/**
 * Merge two attribute objects giving precedence
 * to values in object `b`. Classes are special-cased
 * allowing for arrays and merging/joining appropriately
 * resulting in a string.
 *
 * @param {Object} a
 * @param {Object} b
 * @return {Object} a
 * @api private
 */

exports.merge = function merge(a, b) {
  var ac = a['class'];
  var bc = b['class'];

  if (ac || bc) {
    ac = ac || [];
    bc = bc || [];
    if (!Array.isArray(ac)) ac = [ac];
    if (!Array.isArray(bc)) bc = [bc];
    ac = ac.filter(nulls);
    bc = bc.filter(nulls);
    a['class'] = ac.concat(bc).join(' ');
  }

  for (var key in b) {
    if (key != 'class') {
      a[key] = b[key];
    }
  }

  return a;
};

/**
 * Filter null `val`s.
 *
 * @param {Mixed} val
 * @return {Mixed}
 * @api private
 */

function nulls(val) {
  return val != null;
}

/**
 * Render the given attributes object.
 *
 * @param {Object} obj
 * @param {Object} escaped
 * @return {String}
 * @api private
 */

exports.attrs = function attrs(obj, escaped){
  var buf = []
    , terse = obj.terse;

  delete obj.terse;
  var keys = Object.keys(obj)
    , len = keys.length;

  if (len) {
    buf.push('');
    for (var i = 0; i < len; ++i) {
      var key = keys[i]
        , val = obj[key];

      if ('boolean' == typeof val || null == val) {
        if (val) {
          terse
            ? buf.push(key)
            : buf.push(key + '="' + key + '"');
        }
      } else if (0 == key.indexOf('data') && 'string' != typeof val) {
        buf.push(key + "='" + JSON.stringify(val) + "'");
      } else if ('class' == key && Array.isArray(val)) {
        buf.push(key + '="' + exports.escape(val.join(' ')) + '"');
      } else if (escaped && escaped[key]) {
        buf.push(key + '="' + exports.escape(val) + '"');
      } else {
        buf.push(key + '="' + val + '"');
      }
    }
  }

  return buf.join(' ');
};

/**
 * Escape the given string of `html`.
 *
 * @param {String} html
 * @return {String}
 * @api private
 */

exports.escape = function escape(html){
  return String(html)
    .replace(/&(?!(\w+|\#\d+);)/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/"/g, '&quot;');
};

/**
 * Re-throw the given `err` in context to the
 * the jade in `filename` at the given `lineno`.
 *
 * @param {Error} err
 * @param {String} filename
 * @param {String} lineno
 * @api private
 */

exports.rethrow = function rethrow(err, filename, lineno){
  if (!filename) throw err;

  var context = 3
    , str = require('fs').readFileSync(filename, 'utf8')
    , lines = str.split('\n')
    , start = Math.max(lineno - context, 0)
    , end = Math.min(lines.length, lineno + context);

  // Error context
  var context = lines.slice(start, end).map(function(line, i){
    var curr = i + start + 1;
    return (curr == lineno ? '  > ' : '    ')
      + curr
      + '| '
      + line;
  }).join('\n');

  // Alter exception message
  err.path = filename;
  err.message = (filename || 'Jade') + ':' + lineno
    + '\n' + context + '\n\n' + err.message;
  throw err;
};

  return exports;

})({});

jade.templates = {};
jade.render = function(node, template, data) {
  var tmp = jade.templates[template](data);
  node.innerHTML = tmp;
};

jade.templates["divWithClasses.html"] = function(locals, attrs, escape, rethrow, merge) {
attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
var buf = [];
with (locals || {}) {
var interp;
buf.push('<div class="undefined"></div>');
}
return buf.join("");
}
jade.templates["divWithClasses"] = function(locals, attrs, escape, rethrow, merge) {
attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
var buf = [];
with (locals || {}) {
var interp;
buf.push('<div');
buf.push(attrs({ "class": ("" + (classes) + "") }, {"class":true}));
buf.push('>' + ((interp = data) == null ? '' : interp) + '</div>');
}
return buf.join("");
}
jade.templates["drugTable.html"] = function(locals, attrs, escape, rethrow, merge) {
attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
var buf = [];
with (locals || {}) {
var interp;
buf.push('<table class="table table-bordered table-condensed drug-info following"></table>');
}
return buf.join("");
}
jade.templates["drugTable"] = function(locals, attrs, escape, rethrow, merge) {
attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
var buf = [];
with (locals || {}) {
var interp;
buf.push('<table');
buf.push(attrs({ "class": ('table') + ' ' + ('table-bordered') + ' ' + ('table-condensed') + ' ' + ('drug-info') + ' ' + (isFirstCategoryDrug?'':'following') }, {"class":true}));
buf.push('>');
if ( (isFirstCategoryDrug))
{
buf.push('<tr class="active"><th class="drugName">Name</th><th class="drugBrandName">Brand Names</th><th class="drugMechanism">Mechanism</th><th class="drugUse">Use</th><th class="drugPharmacology">Pharmacology</th><th class="drugSideEffects">Side Effects</th><th class="drugOther">Other</th><th class="drugLinks">Links</th></tr>');
}
buf.push('<tr><td class="drugName"><div class="drugColumn">' + escape((interp = name) == null ? '' : interp) + '<!--i.fa.fa-external-link  more--></div></td><td class="drugBrandName"><div class="drugColumn">' + ((interp = brandName) == null ? '' : interp) + '</div></td><td class="drugMechanism"><div class="drugColumn">' + ((interp = mechanism.text) == null ? '' : interp) + '</div></td><td class="drugUse"><div class="drugColumn">' + ((interp = use.text) == null ? '' : interp) + '</div></td><td class="drugPharmacology"><ul class="pharmacology drugColumn">');
if ( (pharmacology.absorption.text))
{
buf.push('<li class="drugPharmacologyAbsorption"><strong>Absorption</strong><div>' + ((interp = pharmacology.absorption.text) == null ? '' : interp) + '</div></li>');
}
if ( (pharmacology.distribution.text))
{
buf.push('<li class="drugPharmacologyDistribution"><strong>Distribution</strong><div>' + ((interp = pharmacology.distribution.text) == null ? '' : interp) + '</div></li>');
}
if ( (pharmacology.metabolism.text))
{
buf.push('<li class="drugPharmacologyMetabolism"><strong>Metabolism</strong><div>' + ((interp = pharmacology.metabolism.text) == null ? '' : interp) + '</div></li>');
}
if ( (pharmacology.excretion.text))
{
buf.push('<li class="drugPharmacologyExcretion"><strong>Excretion</strong><div>' + ((interp = pharmacology.excretion.text) == null ? '' : interp) + '</div></li>');
}
buf.push('</ul></td><td class="drugSideEffects"><div class="drugColumn">' + ((interp = sideEffects.text) == null ? '' : interp) + '</div></td><td class="drugOther"><div class="drugColumn">' + ((interp = other.text) == null ? '' : interp) + '</div></td><td class="drugLinks"><div class="drugColumn">');
// iterate links
;(function(){
  if ('number' == typeof links.length) {
    for (var $index = 0, $$l = links.length; $index < $$l; $index++) {
      var link = links[$index];

buf.push('<a');
buf.push(attrs({ 'href':("" + (link.url) + ""), 'target':("__blank") }, {"href":true,"target":true}));
buf.push('>&nbsp;' + escape((interp = link.name) == null ? '' : interp) + '</a>');
    }
  } else {
    for (var $index in links) {
      var link = links[$index];

buf.push('<a');
buf.push(attrs({ 'href':("" + (link.url) + ""), 'target':("__blank") }, {"href":true,"target":true}));
buf.push('>&nbsp;' + escape((interp = link.name) == null ? '' : interp) + '</a>');
   }
  }
}).call(this);

buf.push('</div></td></tr></table>');
}
return buf.join("");
}
jade.templates["listItems.html"] = function(locals, attrs, escape, rethrow, merge) {
attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
var buf = [];
with (locals || {}) {
var interp;
buf.push('<ul><each>data</each></ul>');
}
return buf.join("");
}
jade.templates["listItems"] = function(locals, attrs, escape, rethrow, merge) {
attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
var buf = [];
with (locals || {}) {
var interp;
buf.push('<ul');
buf.push(attrs({ "class": ("" + (listClass) + "") }, {"class":true}));
buf.push('>');
// iterate items
;(function(){
  if ('number' == typeof items.length) {
    for (var $index = 0, $$l = items.length; $index < $$l; $index++) {
      var item = items[$index];

buf.push('<li');
buf.push(attrs({ 'id':("" + (item._id) + "") }, {"id":true}));
buf.push('>' + ((interp = item.data) == null ? '' : interp) + '</li>');
    }
  } else {
    for (var $index in items) {
      var item = items[$index];

buf.push('<li');
buf.push(attrs({ 'id':("" + (item._id) + "") }, {"id":true}));
buf.push('>' + ((interp = item.data) == null ? '' : interp) + '</li>');
   }
  }
}).call(this);

buf.push('</ul>');
}
return buf.join("");
}
jade.templates["treeButtons"] = function(locals, attrs, escape, rethrow, merge) {
attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
var buf = [];
with (locals || {}) {
var interp;
buf.push('<ul>');
// iterate buttons
;(function(){
  if ('number' == typeof buttons.length) {
    for (var $index = 0, $$l = buttons.length; $index < $$l; $index++) {
      var button = buttons[$index];

buf.push('<li><label><input');
buf.push(attrs({ 'type':("checkbox"), "class": (button.class) }, {"type":true,"class":true}));
buf.push('/>&nbsp;' + escape((interp = button.name) == null ? '' : interp) + '</label></li>');
    }
  } else {
    for (var $index in buttons) {
      var button = buttons[$index];

buf.push('<li><label><input');
buf.push(attrs({ 'type':("checkbox"), "class": (button.class) }, {"type":true,"class":true}));
buf.push('/>&nbsp;' + escape((interp = button.name) == null ? '' : interp) + '</label></li>');
   }
  }
}).call(this);

buf.push('</ul>');
}
return buf.join("");
}