/* global Q, jade, this */
jade.compile = function (template, data) {
    return jade.templates[template](data);
};

(function (Q, jade) {

    var root = this;

    var isObject = function (obj) {
        return obj === Object(obj); 
    };

    var isBoolean = function (obj) {
        return obj === true || obj === false || toString.call(obj) == '[object Boolean]';
    };

    var isEmpty = function (obj) {
        if (isObject(obj) && obj.length) return obj.length == 0; 
        return obj == null || obj == "";
    };

    var extend = function (destination, source) {
        for (var prop in source)
            if (source.hasOwnProperty(prop))
                destination[prop] = source[prop];
    };

    var capitalize = function (name) {
        return name[0].toUpperCase() + name.substring(1);
    };

    var selectById = function (id) {
        return document.querySelector("#" + id);
    };

    var selectByClass = function (className) {
        return document.querySelectorAll("." + className);
    };

    var hashCode = function (string) {
        var hash = 0, 
            length = string.length,
            char;
        if (length == 0) return hash;
        for (var i = 0; i < length; i++) {
            char = string.charCodeAt(i);
            hash = ((hash << 5) - hash) + char;
            hash = hash & hash; // Convert to 32bit integer
        }
        return hash.toString();
    };


    var JSTree = function () {
        this.deferred = Q.defer();
        this.deferred.resolve();
        this.filtered = [];
        this.params = {
            highlightDirectMatches: false,

            BRANCH_PREFIX: "tree_",
            BRANCH_DIV: "branchDiv",
            CATEGORY_DIV: "categoryDiv",
            DRUG_DIV: "drugDiv",
            HIDDEN: "hidden",
            DIRECT_MATCH: "directMatch",

            DRUG_COLUMN_PREFIX: "drug",
            // Web worker path
            WORKER_NAME: "javascripts/jsTree/worker.js",
            // Template names
            DIV_WITH_CLASSES: "divWithClasses",
            DRUG_TABLE: "drugTable",
            LIST_ITEMS: "listItems"
        };
    };

    JSTree.prototype.shouldHighlightDirectMatches = function () {
        return this.params.highlightDirectMatches == true;
    };

    JSTree.prototype.setParameters = function () {
        return this.then(function (params) {
            extend(this.params, params);
        }, arguments);
    };

    JSTree.prototype.then = function () {

        // [func, shouldResolve] or [func, args, shouldResolve]    
        if (arguments.length > 3) throw new Error("then: more than 3 arguments passed");

        var self = this,
            args = arguments,
            funcToCall = args[0], 
            funcArgs = [],
            shouldResolve = true,
            lastArg = args[args.length-1],
            deferred;

        if (isBoolean(lastArg)) shouldResolve = lastArg;
        // the additional args
        if (isObject(args[1])) funcArgs = Array.prototype.slice.call(args[1]);

        // Make new deferred
        if (!shouldResolve) deferred = Q.defer();

        // Call next function
        this.deferred.promise.then(function () {
            // if (shouldResolve) { deferred.resolve(); } else { funcArgs.push(deferred); }
            if (!shouldResolve) funcArgs.push(deferred);
            funcToCall.apply(self, funcArgs);
        });
        
        // Rewire deferred to point to new deferred
        if (!shouldResolve) this.deferred = deferred;

        return this;
    };

    // An expression to match value. 
    // Removing all RegEx groups ( [],{},() ) to get proper results.
    // "Global" g flag - this.regEx.lastIndex = 0
    JSTree.prototype.addRegEx = function () {
        return this.then(function (value) {
            this.regEx = new RegExp(value.trim().replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'i');
        }, arguments);
    };

    JSTree.prototype.addTree = function () {
        return this.then(function (tree, deferred) {
            this.tree = tree;
            deferred.resolve();
        }, arguments, false);
    };

    JSTree.prototype.addObject = function () {
        return this.then(function (domObject) {
            this.object = domObject;
        }, arguments);
    };

    JSTree.prototype.getTree = function () {
        return this.tree;
    };

    JSTree.prototype.setupFilterWorker = function (deferred) {
        var self = this;
        this.filterWorker = new Worker(self.params.WORKER_NAME);
        this.filterWorker.onmessage = function (oEvent) {
            self.tree = oEvent.data.tree;
            // Save already filtered data
            // console.log("Woot! From worker", self, oEvent.data, oEvent.data.tree[1].visible, self.tree);
            self.saveFiltered(oEvent.data.filter, self.tree);
            deferred.resolve();
        };
        return this;
    };

    JSTree.prototype.saveFiltered = function (filter, data) {
        this.filtered[this.params.BRANCH_PREFIX + filter] = data;
    };

    JSTree.prototype.isFiltered = function (filter) {
        return !isEmpty(this.getFiltered(filter));
    };

    JSTree.prototype.getFiltered = function (filter) {
        return this.filtered[this.params.BRANCH_PREFIX + filter];
    };

    JSTree.prototype.filterTree = function () {
        return this.then(function filter (filter, deferred) {
            if (this.isFiltered(filter)) {
                // Already filtered, return
                this.tree = this.getFiltered(filter);
                deferred.resolve();
            } else {
                // Haven't filtered before, spawn worker            
                this.setupFilterWorker(deferred);
                this.filterWorker.postMessage({ tree: this.tree, filter: filter });
            }
        }, arguments, false);
    };

    JSTree.prototype.renderList = function (data, listHidden) {
        var self = this,
            listClass = (!listHidden ? 'treeList ' + self.params.HIDDEN : 'treeList');
        return jade.compile(self.params.LIST_ITEMS, {
            listClass: listClass,
            items: data.map(function (item) { return { _id: item.getId(), data: item.renderHTML() }; })
        });
    };

    JSTree.prototype.renderDivWithClass = function (data, classes) {
        var self = this;
        if (!classes) classes = "";
        return jade.compile(
            self.params.DIV_WITH_CLASSES, { classes: classes, data: data }
        );
    };

    JSTree.prototype.renderDrugTable = function (drug) {
        var self = this;
        return jade.compile(self.params.DRUG_TABLE, drug);
    };

    JSTree.prototype.renderTree = function () {
        return this.then(function render (deferred) {

            if (isEmpty(this.treeData)) {
                // Never built before
                this.treeData = this.buildObjectsFromTree(this.tree);
                document.querySelector(this.object).innerHTML = this.renderList(this.treeData, true);
                this.setupTreeDataEvents(this.treeData);
            } else {
                this.expandByVisible(this.tree, this.treeData);
            }

            deferred.resolve();
        }, arguments, false);
    };

    JSTree.prototype.buildObjectsFromTree = function (tree) {
        // Builds Category and Drug Objects out of tree branches
        var self = this,
            treeData = [],
            object;

        tree.forEach(function (branch) {
            if (branch.isDrug) {
                // Drug
                object = self.getDrugObject(branch);
                treeData.push(object);
            } else {
                object = self.getCategoryObject(branch);
                // Category
                if (branch.children) {
                    // Category Children
                    childrenData = self.buildObjectsFromTree(branch.children);
                    if (childrenData) {
                        object.children = childrenData;
                    }
                }
                treeData.push(object);
            }
        });

        return treeData;
    };

    JSTree.prototype.setupTreeDataEvents = function (treeData) {
        var self = this;
        treeData.forEach(function (branch) {
            if (branch.isCategory() && branch.hasChildren()) {
                self.setupTreeDataEvents(branch.children);
            }
            // We have to make events for drugs also so that when we propagate
            // on a drug's list parent (category list) we can ignore the event and not collapse
            // the drug list.
            selectById(branch.getId()).addEventListener("click", function (e) {
                e.stopPropagation();
                if (branch.isCategory()) branch.toggle();
            }, false);
        });
    };

    JSTree.prototype.setDrugDisplayProperties = function (properties) {
        var self = this;
        properties.forEach(function loopDisplayColumns (property) {
            [].forEach.call(
                selectByClass(self.params.DRUG_COLUMN_PREFIX + capitalize(property.name)),
                function loopDrugColumns (drugColumn) {
                    if (property.show) {
                        drugColumn.classList.remove(self.params.HIDDEN);
                    } else {
                        drugColumn.classList.add(self.params.HIDDEN);
                    }
                }
            );
        });
    };

    JSTree.prototype.getTreeMap = function (tree) {
        var self = this,
            treeMap = [];

        function traverseRecursively (tree) {        
            tree.forEach(function (branch) {
                treeMap[self.params.BRANCH_PREFIX + branch._id] = branch;
                if (!branch.isDrug && !isEmpty(branch.children)) {
                    traverseRecursively(branch.children);
                }
            });
        }

        traverseRecursively(tree);

        return treeMap;
    };

    JSTree.prototype.expandByVisible = function (tree, treeData) {
        var self = this,
            treeMap = this.getTreeMap(tree);

        // modify treeData's visible from new tree (map)
        function setVisibleBasedOnMatches (treeData) {        
            treeData.forEach(function (branch) {
                var treeMapItem = treeMap[branch.getId()];
                branch.setVisible(treeMapItem.visible);
                branch.setDirectMatch(treeMapItem.directMatch);
                if (branch.hasChildren()) {
                    setVisibleBasedOnMatches(branch.children);
                }
            });
        }

        setVisibleBasedOnMatches(treeData);
        self.expandVisible(treeData);

        if (self.shouldHighlightDirectMatches()) {
            self.highlightDirectMatches(treeData);
        } else {
            self.removeHighlightedDirectMatches();
        }
    };

    JSTree.prototype.expandVisible = function (treeData) {
        var self = this;
        treeData.forEach(function (branch) {
            if (branch.isMatching()) {
                branch.expand();
                if (branch.hasChildren()) {
                    self.expandVisible(branch.children);
                }
            } else {
                branch.collapse();
            }
        });
    };

    JSTree.prototype.highlightDirectMatches = function (treeData) {
        var self = this;
        treeData.forEach(function (branch) {
            if (branch.isDirectMatch()) {
                branch.highlightDirectMatch();
            } else {
                branch.dehighlightDirectMatch();
            }
            if (branch.hasChildren()) {
                self.highlightDirectMatches(branch.children);
            }
        });
    };

    JSTree.prototype.removeHighlightedDirectMatches = function () {
        var self = this;
        [].forEach.call(
            selectByClass(self.params.DIRECT_MATCH),
            function (directMatchElement) {
                directMatchElement.classList.remove(self.params.DIRECT_MATCH);
            }
        );
    };

    JSTree.prototype.getCategoryObject = function (data) {
        var self = this;
        return {
            data: data,
            collapsed: false,
            setVisible: function (visible) {
                this.data.visible = visible;
            },
            setDirectMatch: function (directMatch) {
                this.data.directMatch = directMatch;
            },
            hasChildren: function () {
                return !isEmpty(this.children);
            },
            isCategory: function () {
                return true;
            },
            getId: function () {
                return self.params.BRANCH_PREFIX + this.data._id;
            },
            isDrug: function () {
                return false;
            },
            isMatching: function () {
                return this.data.visible == true;
            },
            isDirectMatch: function () {
                return this.data.directMatch == true;
            },
            renderHTML: function () {
                var classes = self.params.BRANCH_DIV + " " + self.params.CATEGORY_DIV;
                if (self.shouldHighlightDirectMatches() && this.isDirectMatch()) {
                    classes += " " + self.params.DIRECT_MATCH;
                }
                return self.renderDivWithClass(this.data.name, classes) + this.renderChildrenHTML();
            },
            renderChildrenHTML: function () {
                return self.renderList(this.children);
            },
            getInnerList: function () {
                return selectById(this.getId() + " > ul");
            },
            getElement: function () {
                return selectById(this.getId());
            },
            highlightDirectMatch: function () {
                this.getElement(" > div").classList.add(self.params.DIRECT_MATCH);
            },
            dehighlightDirectMatch: function () {
                this.getElement(" > div").classList.remove(self.params.DIRECT_MATCH);
            },
            expand: function () {
                this.getElement().classList.remove(self.params.HIDDEN);
                this.getInnerList().classList.remove(self.params.HIDDEN);
            },
            collapse: function () {
                this.getElement().classList.add(self.params.HIDDEN);
            },
            toggle: function () {
                this.getInnerList().classList.toggle(self.params.HIDDEN);
            },
            children: []
        };
    };

    JSTree.prototype.getDrugObject = function (data) {
        var self = this;
        return {
            data: data,
            hasChildren: function () {
                return false;
            },
            setVisible: function (visible) {
                this.data.visible = visible;
            },
            setDirectMatch: function (directMatch) {
                this.data.directMatch = directMatch;
            },
            getId: function () {
                return self.params.BRANCH_PREFIX + this.data._id;
            },
            isCategory: function () {
                return false;
            },
            isDrug: function () {
                return true;
            },
            isMatching : function () {
                return this.data.visible == true;
            },
            isDirectMatch: function () {
                return this.data.directMatch == true;
            },
            renderHTML: function () {
                var classes = self.params.BRANCH_DIV + ' ' + self.params.DRUG_DIV;
                if (self.shouldHighlightDirectMatches() && this.isDirectMatch()) {
                    classes += (' ' + self.params.DIRECT_MATCH);
                }
                return jade.compile(self.params.DIV_WITH_CLASSES, { classes: classes, data: self.renderDrugTable(this.data) });
            },
            getElement: function () {
                return selectById(this.getId());
            },
            highlightDirectMatch: function () {
                this.getElement(" > div").classList.add(self.params.DIRECT_MATCH);
            },
            dehighlightDirectMatch: function () {
                this.getElement(" > div").classList.remove(self.params.DIRECT_MATCH);
            },
            expand: function () {
                this.getElement().classList.remove(self.params.HIDDEN);
            },
            collapse: function () {
                this.getElement().classList.add(self.params.HIDDEN);
            },
            toggle: function () {
                this.getElement().classList.toggle(self.params.HIDDEN);
            }
        };
    };

    JSTree.prototype.matches = function (value) {
        // breaks down an object
        if (isObject(value)) value = JSON.stringify(value);
        var isMatch = this.regEx.exec(value);
        return (isMatch && isMatch.length > 0);
    };

    JSTree.prototype.matchTree = function () {
        return this.then(function () {
            this.traverseTreeForMatch(this.tree);
        });
    };

    JSTree.prototype.traverseTreeForMatch = function (tree, parentHasMatch) {

        var self = this,
            isMatch,
            isDirectBranchMatch,
            hasOneMatched = false;

        tree.forEach(function (branch, i, a) {

            isMatch = (parentHasMatch == true);
            isDirectBranchMatch = false;

            if (branch.isDrug) {
                // Drugs
                for (var prop in branch) {
                    if (isMatch) break;
                    if (branch.hasOwnProperty(prop) && prop != "ancestors") {
                        if (prop == "pharmacology") {
                            // Properties within pharmacology
                            for (var pharProp in branch[prop]) {
                                if (self.matches(branch[prop][pharProp])) {
                                    isMatch = true;
                                    break;
                                }
                            }
                        // properties: [use, name, parents, other, label, 
                        // distribution, absorption, metabolism, etc]
                        } else if (self.matches(branch[prop])) {
                            isMatch = true;
                        }
                    }
                }

                // Drugs always have direct match
                if (isMatch) isDirectBranchMatch = true;

            } else {
                // Categories
                isDirectBranchMatch = self.matches(branch.name);

                if (!isMatch) {
                    isMatch = isDirectBranchMatch; 
                }

                if (branch.children) {
                    // Loop through children elements
                    hasOneChildMatch = self.traverseTreeForMatch(branch.children, isMatch);
                    // If child matches, parent matches as well
                    if (hasOneChildMatch) isMatch = true;
                }
            } 


            if (isMatch) hasOneMatched = true;
            a[i].visible = isMatch;
            a[i].directMatch = isDirectBranchMatch;
        });
        return hasOneMatched;
    };

    root.JSTree = JSTree;

}).call(this, Q, jade);