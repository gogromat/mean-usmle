/* global postMessage, JSTree */

importScripts("templates.js", "q.js", "tree.js");

onmessage = function (oEvent) {
	var data = oEvent.data;

	new JSTree()
	.addTree(data.tree)
	.addRegEx(data.filter)
	.matchTree()
	.then(function () {
		console.log("worker: posting results");
		data.tree = this.getTree();
		postMessage(data);
	});
};