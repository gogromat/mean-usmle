UDApp.factory("appService", ['$q', '$http', '$rootScope', '_', function ($q, $http, $rootScope, _) {

    var appService = {};

    appService.drugCategory = {};

    appService.categories = [];
    appService.wereCategoriesFetched = false;

    appService.drugs = [];

    appService.treeData = [];

    appService.tryGetObjectDeepValue = function (obj, valuePath) {    
        try {
            var splitValuePath = valuePath.split('.'),
                value     = obj;
            for (var i = 0; i < splitValuePath.length; i++) {
                value = value[splitValuePath[i]];
            }
            return value;
        } catch (e) {
            throw new Error("Cannot get non-existent value " + valuePath);
        }
    };

    appService.trySetObjectDeepKeyToValue = function (obj, params) {
        var keyPath  = (params.key ? params.key.split('.') : []),
            value = params.value,
            add   = params.add,
            onlyIfEmpty = params.onlyIfEmpty,
            // addPropertyIfNonExistant = params.addPropertyIfNonExistant || false,
            subObject = obj,
            previousValue;
        
        try {
            
            for (var i = 0; i < keyPath.length -1; i++) {
                
                subObject = subObject[keyPath[i]];
            }
            previousValue = subObject[keyPath[keyPath.length-1]];

            // Checks to set value only if it is empty and there is no value
            if (onlyIfEmpty && !previousValue || !onlyIfEmpty) {            
                if (add) {
                    subObject[keyPath[keyPath.length-1]] += value;
                } else {
                    subObject[keyPath[keyPath.length-1]] = value;
                }
            }

        } catch (e) {
            throw new Error("Cannot set value for non-existant key " + params.key);
        }
    };

    appService.addProtocolToURLIfNeeded = function (protocol, url) {
        if (arguments.length == 1) {
            url = protocol;
            protocol = "";
        } 
        return url.substr(0, 2) == "//" ? this.addProtocolToURL(protocol, url) : url;
    };

    appService.addProtocolToURL = function (protocol, url) {
        return (protocol ? protocol : "http:") + url;
    };

    appService.setDrugCategory = function (drug, category) {
        if (drug && this.drugCategory.drug != drug) {        
            this.drugCategory.drug = drug;
            $rootScope.$broadcast('setDrugCategory');
        }
        if (category && this.drugCategory.category != category) {
            this.drugCategory.category = category;
            $rootScope.$broadcast('setDrugCategory');
        }
    };


    appService.makeCategoriesVisibleInList = function (categories) {
        categories.forEach(function (category, i, a) {
            a[i].expanded = true;
        });
        return categories;
    };

    appService.makeDrugsVisibleInList = function (drugs, categoryType) {
        var prevParent = '';
        drugs.forEach(function (drug, i, a) {
            a[i].isDrug = true;
        });
        return drugs;
    };

    // Drugs must be ordered by parents.parent (category)
    appService.setupDrugFirstParentByCategoryType = function (drugs, categoryType) {


        var previousDrugCategory;

        drugs.forEach(function (drug, drugI, drugArr) {
            drug.parents.forEach(function (parent, parentI, parentArr) {
                
                // we only look at the same category type
                if (parent.categoryType == categoryType._id) {
                    // console.log("Curr:", parent.parent, "Prev:", previousDrugCategory);
                    // if drug has a different category - its a first drug in its category
                    // since all drugs will be rebuild based on category type,
                    // we can add the flag directly to drug or reset this flag if it was set on other category types
                    drugArr[drugI].isFirstCategoryDrug = (previousDrugCategory != parent.parent);
                    previousDrugCategory = parent.parent;
                }
            });
        });

        return drugs;
    };

    appService.setupDrugParentByCategoryType = function (drugs, categoryType) {

        var cTID = categoryType._id;

        drugs.forEach(function (drug, drugI, drugArr) {
            drug.parents.forEach(function (parent) {
                if (parent.categoryType == cTID) {
                    drugArr[drugI].parent = parent.parent;
                }
            });
        });

        return drugs;
    };
        

    // AppService's Drugs
    appService.setDrugs = function (drugs) {
        this.drugs = drugs;
        this.makeDrugsVisibleInList(this.drugs);
        $rootScope.$broadcast('setDrugs');
    };

    // AppService's Categories
    appService.setCategories = function (categories) {
        this.categories = categories;
        $rootScope.$broadcast('setCategories');
    };

    // Broadcast when both categories and drugs are set
    $rootScope.$on("setCategories", function () {
        // console.log("appService::setCategories");
        if (appService.drugs.length > 0 && appService.categories.length > 0) {
            // console.log("appService::: broadcasting setDrugsAndCategories");
            $rootScope.$broadcast('setDrugsAndCategories');
        }
    });
    $rootScope.$on("setDrugs", function () {
        // console.log("appService::setDrugs");
        if (appService.drugs.length > 0 && appService.categories.length > 0) {
            // console.log("appService::: broadcasting setDrugsAndCategories");
            $rootScope.$broadcast('setDrugsAndCategories');
        }
    });

    appService.getCategoryById = function (id, categories) {
        var category, categoriesToSearch = categories || this.categories;
        if (id && categoriesToSearch) {
            categoriesToSearch.some(function (cat) {
                if (cat._id === id) {
                    category = cat;
                    return true;
                }
            });
        }
        return category;
    };


    // Make async and throw promise
    appService.fetchCategories = function (forceUpdate) {

        // console.log("appService::fetchCategories");
        
        var promise, deferred;

        if (!appService.wereCategoriesFetched) {
            forceUpdate = true;
        }

        if (forceUpdate) {
            // console.log("Fetching categories POST");
            promise = $http({
                url: '/api/categories',
                method: "POST",
                headers: {'Accept': 'application/json'}
            }).success(function (data, status, headers, config) {
                
                // console.log("Actual POST data");

                appService.setCategories(data[0].categories);
                appService.wereCategoriesFetched = true;
                return data[0].categories;
            }).error(function (data, status, headers, config) {
                return { "error": true };
            });
        } else {
            // console.log("Fetching categories DEFERRED");
            deferred = $q.defer();
            setTimeout(function() {
                deferred.resolve(appService.categories);
            }, 0);
            promise = deferred.promise;
        }
        return promise;
    };


    appService.getCategoriesTree = function (additionalData, categories) {
        var safeCopyCategories = angular.copy(categories || this.categories), result;
        
        this.makeCategoriesVisibleInList(safeCopyCategories);
        result = this.buildTree(safeCopyCategories);
        
        if (additionalData) result = result.concat(additionalData);
        return result;
    };


    appService.filterTree = function (value) {

        // An expression to match value. 
        // Removing all RegEx groups ( [],{},() ) to get proper results.
        var regEx = new RegExp(value.trim().replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'ig');

        function matches (val) {
            // what if it is not a string, but an object?
            if (_.isObject(val)) val = JSON.stringify(val);

            var isMatch = regEx.exec(val);
            if (!isMatch) return false;
            return isMatch.length > 0;
        }

        function recursiveTreeTraversal (tree, parentBranchHasMatch) {
            
            var isMatch = false,
                isDrugMatch = false,
                isCategoryMatch = false;

            tree.forEach(function (branch, i, a) {

                if (!branch.isDrug) {

                    // Categories
                    isCategoryMatch = false;

                    if (parentBranchHasMatch || matches(branch.name)) {
                        isCategoryMatch = true;
                        if (branch.children) {
                            recursiveTreeTraversal(branch.children, true);
                        }
                    } else if (branch.children) {
                        isCategoryMatch = recursiveTreeTraversal(branch.children);
                    }

                    if (isCategoryMatch) {
                        // At least one category matches
                        isMatch = true;
                        a[i].visible = true;
                    } else {
                        // category does not match, make invisible
                        a[i].visible = false;
                    }

                } else {

                    // Drugs
                    isDrugMatch = false;

                    if (parentBranchHasMatch) {
                        isDrugMatch = true;
                    } else {

                        for (var prop in branch) {
                            if (branch.hasOwnProperty(prop) && prop != "ancestors") {
                                if (prop == "pharmacology") {
                                    for (var pharProp in branch[prop]) {
                                        if (matches(branch[prop][pharProp])) isDrugMatch = true;
                                    }
                                } else if (matches(branch[prop])) {
                                    isDrugMatch = true;
                                }
                            }
                        }
                    }

                    if (isDrugMatch) {
                        // at least one drug matches
                        isMatch = true;
                        a[i].visible = true;
                    } else {
                        // drug does not match, make invisible
                        a[i].visible = false;
                    }

                }

            });

            return isMatch;
        }

        recursiveTreeTraversal(this.treeData);
        return this.treeData;
    };


    // Appending drugs to categories and calls buildTree
    appService.buildTreeWithDrugs = function (categories, drugs, currentCategoryType) {
        
        var categoriesWithDrugs = angular.copy(categories);

        this.makeCategoriesVisibleInList(categoriesWithDrugs);
        this.makeDrugsVisibleInList(drugs);
        
        // setup drug's flags
        drugs = this.setupDrugFirstParentByCategoryType(drugs, currentCategoryType);
        // assign drugs current parent to drug.parent
        drugs = this.setupDrugParentByCategoryType(drugs, currentCategoryType);

        // combine drugs with categories
        categoriesWithDrugs = categoriesWithDrugs.concat(drugs);

        this.treeData = this.buildTree(categoriesWithDrugs);

        appService.removeBranchesWithEmptyDrugs();

//        console.log(JSON.stringify(this.treeData));

        return this.treeData;
    };

    function repeat(pattern, count) {
        if (count < 1) return '';
        var result = '';
        while (count > 1) {
            if (count & 1) result += pattern;
            count >>= 1, pattern += pattern;
        }
        return result + pattern;
    }

    appService.removeBranchesWithEmptyDrugs = function () {

        function recursiveGetCategoriesWithDrugs (categories) {

            if (categories[0].isDrug == true) {
                return categories;
            }

            var index = categories.length;

            while (index--) {

                if (categories[index].children) {
                    // Has children
                    categories[index].children = recursiveGetCategoriesWithDrugs(categories[index].children);
                }

                if (!categories[index].children || categories[index].children.length == 0) {
                    // No children, and it is not a drug, remove category plox
                    categories.splice(index);
                }
            }

            return categories;
        }

        return this.treeData = recursiveGetCategoriesWithDrugs(this.treeData);
    };


    appService.buildTree = function (categories) {

        var tree = [],
            loopedOnce = false;

        recursiveCategoriesLoop(categories);

        // parent categories may not be set until the very end,
        // that is why we keep looping until they are all set
        function recursiveCategoriesLoop (categories) {

            // remove empty categories
            categories = categories.filter(function (i) { return i; });

            // Continue traversing the rest of the categories
            // and populating sub-levels of the tree
            categories.forEach(function (category, i, a) {
                
                // On the first loop do some assignments
                if (!loopedOnce) {
                    // copy name
                    a[i].label = category.name || "default";
                    
                    // by default show all
                    a[i].visible = true;

                    // How many times we looped this category over
                    a[i].looped = 1;
                }


                if (!a[i].parent) {
                    
                    // First-Level categories
                    
                    tree.push(a[i]);
                    delete(a[i]);

                } else if (loopedOnce) {
                    
                    // The rest, after we looped once and pushed all
                    // of the first-level categories down

                    // Find parent category in a tree
                    var treeItem = recursiveTreeTravesal(tree, a[i].parent);

                    if (treeItem) {
                        
                        // If parent category is found in a tree, link this category
                        // to the parent's children categories
                        
                        if (!treeItem.children) treeItem.children = [];
                        treeItem.children.push(a[i]);
                        // If we found parent right away, delete category
                        delete(a[i]);
                    } else {

                        // No parent found, increment count of this sadness
                        
                        a[i].looped++;
                        console.log("Looping again for:", a[i]);

                        if (a[i].looped > 40) {
                            // We looped too many times, running call stack thin,
                            // time to get rid of this category
                            console.log("Removing item in order not no deplete memory stack. Item:", a[i]);
                            delete(a[i]);
                        }

                    }
                }
                
            });

            // we just looped once :)
            loopedOnce = true;
            
            // If there are still categories left, loop over them
            if (categories.length > 0) {
                recursiveCategoriesLoop(categories);
            }
        }

        // locate the parent of the item by itemParam provided
        function recursiveTreeTravesal (tree, itemParam) {

            var match = false;
            
            tree.forEach(function (treeItem, i, a) {
                if (!match) {
                    // If matches this item, return this item
                    if (treeItem._id.toString() == itemParam.toString()) {
                        match = treeItem;
                        return match;
                    }
                    // If matches this item's child, return the child item
                    if (treeItem.children) {
                        childrenMatch = recursiveTreeTravesal(treeItem.children, itemParam);
                        if (childrenMatch) match = childrenMatch;
                    }
                }
            });
            
            return match;
        }

        return tree;

    };

    return appService;
}]);