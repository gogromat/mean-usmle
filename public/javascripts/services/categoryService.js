UDApp.factory("categoryService", ['$q', '$http', '$rootScope', '_', function ($q, $http, $rootScope, _) {

    var categoryService = {},
        categoryServiceCategories = [];

    categoryService.setCategories = function (cs) {
        categoryServiceCategories = cs;
        $rootScope.$broadcast("categoryService.setCategories");
    };

    categoryService.getCategories = function () {
        return categoryServiceCategories;
    };

    categoryService.getCategoryById = function (categoryId) {
        var category = {};
        categoryServiceCategories.forEach(function (ct) {
            if (ct._id == categoryId) {
                category = ct;
            }
        });
        return category;
    };

    /**
     * Returns categories that belong to particular category type
     */
    categoryService.getCategoriesMatchingCategoryTypeCategories = function (categoryType) {

        var categoryTypeCategories = categoryType.categories,
            resultCategories = [];

        // Loops through all categories that exist
        // and if their category ids match the ones from category type
        // push those categories to results
        categoryServiceCategories.forEach(function (c) {
            if (categoryTypeCategories.indexOf(c._id) != -1) {
                c.collapsed = true;
                resultCategories.push(_.clone(c));
            }
        });

        return resultCategories;
    };

    return categoryService;
}]);