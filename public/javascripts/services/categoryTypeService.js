UDApp.factory("categoryTypeService", ['$q', '$http', '$rootScope', '_', function ($q, $http, $rootScope, _) {

    var categoryTypeService = {},
        categoryTypes = [],
        currentCategoryType,
        currentCategoryTypeCategories;

    categoryTypeService.getCategoryTypes = function () {
        return categoryTypes;
    };

    categoryTypeService.getCurrentCategoryType = function () {
        return currentCategoryType;
    };

    categoryTypeService.getCategoryTypeById = function (categoryTypeId) {
        var categoryType = {};
        categoryTypes.forEach(function (ct) {
            if (ct._id == categoryTypeId) categoryType = ct;
        });
        return categoryType;
    };

    categoryTypeService.setCategoryTypes = function (cts) {
        categoryTypes = cts;
        $rootScope.$broadcast("categoryTypeService.setCategoryTypes");
    };

    categoryTypeService.setCurrentCategoryType = function (ct) {
        categoryTypes.forEach(function (categoryType) {
            if (ct.name == categoryType.name) {
                currentCategoryType = ct;
            }
        });
        $rootScope.$broadcast("categoryTypeService.setCurrentCategoryType");
    };

    return categoryTypeService;
}]);