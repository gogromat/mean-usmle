UDApp.factory("dbService", ['$q', '$http', '$upload', '$rootScope', 
    '_', 'appService', 'flashService', 'helperService',
    function ($q, $http, $upload, $rootScope, _, appService, flashService, helperService) {

    var dbService = {
        collections : [
            { single: "user",         plural: "users"        }, 
            { single: "drug",         plural: "drugs"        },
            { single: "category",     plural: "categories"   },
            { single: "categoryType", plural: "categoryTypes"}
        ],
        actions     : ["delete", "update", "add", "get", "obtain"],
        fields: {
            "drug": [
                { name: "_id",                          searchable: false, sortable : true  },
                { name: "name",                         searchable: true,  sortable : true  },
                { name: "brandName",                    searchable: true,  sortable : true  },
                { name: "isProto",                      searchable: false, sortable : true  },
                { name: "mechanism.text",               searchable: true,  sortable : true  },
                { name: "use.text",                     searchable: true,  sortable : false },
                { name: "sideEffects.text",             searchable: true,  sortable : false },
                { name: "other.text",                   searchable: true,  sortable : false },
                { name: "pharmacology.absorption.text", searchable: true,  sortable : false },
                { name: "pharmacology.distribution.text", searchable: true, sortable: false },
                { name: "pharmacology.metabolism.text", searchable: true,  sortable : false },
                { name: "pharmacology.excretion.text",  searchable: true,  sortable : false },
                { name: "pharmacology.other.text",      searchable: true,  sortable : false },
                { name: "disabled",                     searchable: true,  sortable : false }
            ]
        },
        getSortableFields : function (collection_name) {
            var allItems = dbService.fields[collection_name];
            return _.filter(allItems, 'sortable');
        },
        getSearchableFields : function (collection_name) {
            var allItems = dbService.fields[collection_name];
            return _.filter(allItems, 'searchable');
        }
    };

    // DELETE => delete => api/[name]/:id
    // PUT    => update => api/[name]/:id + data
    // POST   => add    => api/[name]/new + data
    // GET    => get    => api/[name] + params

    // getDrug(:id, cb)
    // deleteCategory(category, cb)

    dbService.collections.forEach(function (collection) {
        var capitalizedSingle = helperService.capitalize(collection.single),
            capitalizedPlural = helperService.capitalize(collection.plural);

        dbService.actions.forEach(function (action) {
            dbService[action + capitalizedSingle] = function (data, cb, params) {
                return dbService[action](collection.plural, data, cb, params);
            };
            dbService[action + capitalizedPlural] = function (data, cb, params) {
                return dbService[action](collection.plural, data, cb, params);
            };
        });
    });

    dbService.delete = function (name, item, cb, params) {
        return $http({
            url: '/api/' + name + '/' + item._id,
            method: "DELETE",
            headers: {'Accept': 'application/json'}
        }).success(function (data, status, headers, config) {
            finallyCallback(data, status, headers, config, cb);
        }).error(function (data, status, headers, config) {
            finallyCallback(data, status, headers, config, cb);
        });
    };

    dbService.update = function (name, item, cb, params) {
        var data = {};
        if (item)   data.item = item;
        if (params) _.extend(data, params);
        return $http({
            url     : '/api/' + name + '/' + item._id,
            method  : "PUT",
            data    : data
        }).success(function (data, status, headers, config) {
            finallyCallback(data, status, headers, config, cb);
        }).error(function (data, status, headers, config) {
            finallyCallback(data, status, headers, config, cb);
        });
    };

    dbService.add = dbService.post = function (name, item, cb, params) {
        var data = {},
            url = '/api/' + name;
        if (item)   data.item = item;
        if (params) _.extend(data, params);

        if (!data.notAddingNew) url += '/new';

        return $http({
            url     : url,
            method  : "POST",
            headers : {'Accept': 'application/json'},
            data    : data
        }).success(function (data, status, headers, config) {
            finallyCallback(data, status, headers, config, cb);
        }).error(function (data, status, headers, config) {
            finallyCallback(data, status, headers, config, cb);
        });
    };

    dbService.obtain = function (name, item, cb, params) {
        // Since you cannot send JSON reliably through GET,
        // you have to use other methods. It is a somewhat
        // violation to RESTful API, but at least this is more
        // reliable way of sending data.
        return dbService.post(name, item, cb,
            _.extend(params || {}, {notAddingNew :true })
        );
    };

    dbService.get = function (name, urlParams, cb) {
        return $http({
            url     : "/api/" + name + (urlParams || ""),
            method  : "GET",
            headers : { 'Accept': 'application/json' }
        }).success(function (data, status, headers, config) {
            finallyCallback(data, status, headers, config, cb);
        }).error(function (data, status, headers, config) {
            finallyCallback(data, status, headers, config, cb);
        });
    };

    function finallyCallback (data, status, headers, config, cb) {
        var params = {
            error  : data,
            data   : data,
            status : status,
            headers: headers,
            config : config
        };
        if (data.success && data.success == true) params.error = null;
        if (cb) cb(params);
    }



    dbService.scrapeAddress = function (item, address, alternativeAddress, cb) {
        return $http({
            url     : '/api/scrape/' + address,
            method  : "POST",
            data    : {
                data : {
                    item: item,
                    alternativeAddress: alternativeAddress
                }
            }
        }).success(function (data, status, headers, config) {
            finallyCallback(data, status, headers, config, cb);
        }).error(function (data, status, headers, config) {
            finallyCallback(data, status, headers, config, cb);
        });
    };
    
        // FILES
    dbService.addImageFileForItem = function (item, $files, $event, cb) {

        if (!$files || $files.length == 0) {

            var imageURL = appService.addProtocolToURLIfNeeded(item.image.text);
            // console.log(imageURL);

            return $http({
                url     : '/api/images/new',
                method  : "POST",
                data    : { 
                    imageUrl : imageURL
                }
            }).success(function (data, status, headers, config) {
                // console.log(data.file);
                flashService.flash('success', "Image saved to: " + data.file);
                item.image.text = data.file;
                
                finallyCallback(data, status, headers, config, cb);

            }).error(function (data, status, headers, config) {
                console.log("Error:" + data);
                flashService.flash('error', data);
                item.image.text = "/images/no_image.gif";

                finallyCallback(data, status, headers, config, cb);
            });

        } else {

            for (var i = 0; i < $files.length; i++) {
                var file = $files[i];

                $upload = $upload.upload({
                    url  : '/api/images/new',
                    file : file
                }).success(function (data, status, headers, config) {
                    if (!data.file) flashService.flash('error', "Could not save image");
                    item.image.text = data.file;

                    finallyCallback(data, status, headers, config, cb);

                }).error(function (data, status, headers, config) {
                    console.log("Error:" + data);
                    flashService.flash('error', data);
                    item.image.text = "/images/no_image.gif";

                    finallyCallback(data, status, headers, config, cb);
                });
                //.progress(function(evt) { 
                // console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total)); 
                // });
            }
        }
    };

    return dbService;
}]);