UDApp.factory("drugService", ['$q', '$http', '$rootScope', '_', 'appService', function ($q, $http, $rootScope, _, appService) {

    var drugService = {},
        drugs = [],
        drugsByMode = [];

    drugService.setDrugs = function (ds) {
        drugs = ds;
        $rootScope.$broadcast("drugService.setDrugs");
    };

    drugService.getDrugs = function () {
        return drugs;
    };

    drugService.getDrugById = function (drugId) {
        var drug = {};
        drugs.forEach(function (d) {
            if (d._id == drugId) drug = d;
        });
        return drug;
    };

    drugService.setDrugDefaultFields = function (drug) {
        if (!drug.pharmacology)
            drug.pharmacology = [];
        if (!drug.brandName)
            drug.brandName = "";
        
        [
          "mechanism", 
          "use", 
          "sideEffects", 
          "image",
          "other", 
          "pharmacology.absorption",
          "pharmacology.distribution", 
          "pharmacology.metabolism", 
          "pharmacology.excretion", 
          "pharmacology.other"
        ].forEach(function (prop) {
            appService.trySetObjectDeepKeyToValue(drug, {
                key: prop, 
                value: { text: "", likeProto: false },
                onlyIfEmpty: true
            });
        });

        if (!drug.data)
            drug.data = {};
    };

    // Sets default parents for drug if parent by certain category type id does not exist
    drugService.setDefaultParentObjectsByCategoryTypeIds = function (parents, categoryTypeIds) {
        
        if (!parents) parents = [];

        var categoryTypeIdsThatAreSet = _.pluck(parents, 'categoryType'),
            // Finds unset parent category type ids
            categoryTypeIdsToSet = _.difference(categoryTypeIds, categoryTypeIdsThatAreSet);

        // Sets the default parent object for unset category type ids
        categoryTypeIdsToSet.forEach(function (categoryTypeId) {
            parents.push({
                categoryType : categoryTypeId,
                ancestors    : [],
                parent       : ""
            });
        });

        return parents;
    };

    drugService.getDrugsByCategoryType = function (categoryType, theDrugs) {
        
        var categoryTypeDrugs = [],
            allDrugs = theDrugs || drugs;

        allDrugs.forEach(function (drug) {
            if (drug.disabled) {
                return;
            }
            drug.parents.forEach(function (parent) {
                if (parent.categoryType == categoryType._id) {
                    categoryTypeDrugs.push(drug);
                }
            })
        });

        return categoryTypeDrugs;

    };



    /*
     * Changed the drugs based on the mode (modifies show properties)
     * TODO: change caching
     */
    drugService.changeDrugsByMode = function (drugs, newMode, oldMode) {
        
        // drugs should be ordered by parentId as well as group prototype beforehand
        var modeDrugs = [], 
            sameGroup = false,
            groupPrototypeDrug = {},

            walkPropertiesRecursively = function (object, passCb, parentPropertyName) {
                var innerPropertyName;
                for (var prop in object) {
                    if (object.hasOwnProperty(prop)) {
                        innerPropertyName = (parentPropertyName ? parentPropertyName + "." : "") + prop;
                        if (passCb(object[prop], innerPropertyName)) {
                            walkPropertiesRecursively(object[prop], passCb, innerPropertyName);
                        }
                    }
                }
            };

        if (!drugsByMode[oldMode]) drugsByMode[oldMode] = angular.copy(drugs);
        if (drugsByMode[newMode]) return drugsByMode[newMode];

        if (newMode == "study") {

            drugs.forEach(function (drug) {

                if (drug.isProto) {
                    groupPrototypeDrug = drug;
                }
                
                // drug is in same group with its group proto and it is not a proto drug itself :p
                if (groupPrototypeDrug != drug && groupPrototypeDrug.parent == drug.parent) {

                    // copy over proto just in case :)
                    drug.groupPrototype = groupPrototypeDrug;

                    walkPropertiesRecursively(drug, function walkMore (obj, topObjPropName) {
                        
                        if (_.isPlainObject(obj)) {

                            if (obj.likeProto) {
                                
                                // these drugs have same property text
                                // obj.text = groupPrototypeDrug[topObjPropName].text;
                                obj.text = "Same as " + groupPrototypeDrug.name;
                                // does not need to walk more
                                return false;
                            }
                            // needs to walk over inner object
                            return true;
                        }
                        return false; 
                    });
                }

                modeDrugs.push(drug);
            });
            drugsByMode[newMode] = modeDrugs;

        } else if (newMode == "display") {
            // do smth if necessary
        } else {
            // do smth if necessary
        }
        
        return drugsByMode[newMode];
    };

    return drugService;
}]);