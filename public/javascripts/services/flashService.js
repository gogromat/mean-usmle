UDApp.factory("flashService", ['$http', 'flash', function ($http, angularFlash) {

    var flashService = {};

    flashService.messagesUrl = "/api/messages";

    flashService.flash = function (status, message) {

        if (status || message) {

            if (!message) {
                // status is message
                angularFlash('success', status);
            } else {
                angularFlash(status, message);
            }
            
        } else {

            // Fetch all application flash messages and displays them        
            this.getApplicationFlashMessages(function (err, messages) {
                if (err) {
                    angularFlash('error', err);
                } else {
                    for (var status in messages) {
                        // c'mon, of course its their own property
                        //if (messages.hasOwnProperty(status)) {
                        messages[status].forEach(function (message) {
                            angularFlash(status, message);
                        });
                        //}
                    }
                }
            });
        }
    };

    flashService.error = function (message) {
        flashService.flash("error", message);
    };

    flashService.getApplicationFlashMessages = function (cb) {
        $http({
            url: flashService.messagesUrl,
            method: "GET"
            //headers: {'Accept': 'application/json'}
        }).success(function (data) {
            cb(null, data.messages);
        }).error(function(err) {
            cb(err);
        });    
    };

    return flashService;
}]);