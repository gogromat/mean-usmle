UDApp.factory("helperService", ["_", function (_) {

    var helperService = {
    };

    helperService.toType = function(obj) {
      return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
    };

    helperService.capitalize = function (string) {
        return string[0].toUpperCase() + string.substring(1);
    };

    helperService.convertObjectToURLComponent = function (obj) {
        return "?" + Object.keys(obj).reduce(function (arr, key) {
            if (_.isArray(obj[key])) {
                obj[key].forEach(function (objValue) {
                    arr.push(key + "[]=" + encodeURIComponent(objValue));
                });
            } else {
                arr.push(key + '=' + encodeURIComponent(obj[key]));
            }
            return arr;
        }, []).join('&');
    };

    helperService.getDomainFromURL = function (url) {
        var doubleSlashPos = url.indexOf("//") + 2,
            afterDomainStart = url.indexOf("/", doubleSlashPos);
        return url.substr(0, afterDomainStart);
    };

    helperService.stripDomainFromURL = function (url) {
    	var doubleSlashPos = url.indexOf("//") + 2,
    		afterDomainStart = url.indexOf("/", doubleSlashPos);
    	return url.substr(afterDomainStart, url.length);
    };

    helperService.stripQueryFromURL = function (url) {
        var queryBeginning = url.indexOf("?");
        if (queryBeginning < 0) queryBeginning = url.indexOf("&");
        return (queryBeginning < 0 ? url : url.substr(0, queryBeginning));
    };

    helperService.getPathFromURL = function (url) {
        return helperService.stripQueryFromURL(helperService.stripDomainFromURL(url));
    };

    helperService.makeObjectPropertiesAsIntegersIfTheyAreIntegers = function (obj) {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                if (helperService.isInt(obj[prop])) {
                    obj[prop] = parseInt(obj[prop], 10);
                }
            }
        }
    };

    helperService.makeObjectPropertiesAsBooleansIfTheyAreBooleans = function (obj) {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                if (obj[prop] == "true")  obj[prop] = true;
                if (obj[prop] == "false") obj[prop] = false;
            }
        }
    };

    helperService.isEqual = function (objA, objB) {
        helperService.makeObjectPropertiesAsIntegersIfTheyAreIntegers(objA);
        helperService.makeObjectPropertiesAsIntegersIfTheyAreIntegers(objB);
        // if same, return true
        return (_.isEqual(objA, objB));
    };

    helperService.isInt = function (num) {
        return num == parseInt(num, 10);
    };

    helperService.range = function (start, stop, step){
        var a=[start], b=start;
        while(b<stop){b+=step;a.push(b)}
        return a;
    };

    helperService.verifyEmail = function (email) {
        return email && email.match(/\w+@\w+.\w+/i);
    };

//    helperService.updateItemInCollection = function (item, collection) {
//        collection[collection.indexOf(item)]
//    };

    return helperService;
}]);