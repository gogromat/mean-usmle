UDApp.factory("pagingService", ['$q', '$http', '$rootScope', '_', 'dbService', 'helperService', function ($q, $http, $rootScope, _, dbService, helperService) {

    var defaultPager = {
        filter : {
            totalPages    :    10,
            page          :     1,
            sortField     : "_id",
            sortDirection :     1,
            items         :   200,
            totalItems    :     0,
            limit         :   200,
            findFields    :    [],
            findParam     : ""
        }
    },
        filterKeys = _.keys(defaultPager.filter);


    // pager should be exposed to controller and views
    // ex.: $scope.pager = pagingService.pager
    var pagingService = {
        pager : {

            findParam: "",

            filter: {
                // extends from defaultPager
                // adds findFields from pager's setFilterFindFields
            },
            sortDirections: [
                { dir :  1, name : 'ASC' }
            ],
            findFields : [],
            sortKeys   : [],

            setFilterFindFields: function () {
                // sets the pager's filter findFields
                // with keys that have been selected by user in a view
                // { name: "findKey", value: "findValue" }

                pagingService.pager.filter.findFields = _.map(
                    _.pluck(
                        _.filter(pagingService.pager.findFields,
                            function(field) { return field.selected; }
                        ),
                        'name'
                    ),
                    function (field) {
                        return { name: field, value: pagingService.pager.filter.findParam };
                    }
                );

            },

            getItemsOnPageNumber: function () {
                var itemsTotal = this.filter.page * this.filter.limit;
                if (itemsTotal > this.filter.totalItems) {
                    return this.filter.limit - (itemsTotal - this.filter.totalItems);
                }
                return this.filter.limit;
            }
        }
    };

    _.extend(pagingService.pager, defaultPager);

    pagingService.setFilter = function (newFilter) {

        helperService.makeObjectPropertiesAsIntegersIfTheyAreIntegers(newFilter);
        helperService.makeObjectPropertiesAsBooleansIfTheyAreBooleans(newFilter);

        // only pick filter keys and re-assign them to pager filter
        _.extend(pagingService.pager.filter,
            pagingService.pickOnlyFilterRelatedKeysFromObject(newFilter)
        );
    };
    pagingService.setPage = function (page) {
        pagingService.pager.filter.page = page;
    };

    pagingService.setSortFields = function (keys) {
        pagingService.pager.sortKeys = keys;
    };

    pagingService.setFindFields = function (keys) {
        pagingService.pager.findFields = [];
        keys.forEach(function (key) {
            pagingService.pager.findFields.push(
                _.extend({}, key, { selected: false })
            );
        });
    };

    pagingService.pickOnlyFilterRelatedKeysFromObject = function (object) {
        return _.pick(object, function (v, k) { return filterKeys.indexOf(k) > -1; });
    };



    pagingService.get = function (name, data, cb) {
        // usually uses pager.filter to get information
        if (!cb && typeof data == "function") {
            cb = data;
            data = pagingService.pager.filter;
        }
        dbService.obtain(name, null, function resultsFromDbServiceGet (results) {
            // Update filter values hot from db or
            // Rollback to default pager in case of a failure :s
            if (results.error) {
                pagingService.setFilter(defaultPager.filter);
            } else {
                pagingService.setFilter(results.data.filter);
            }
            cb(results);

        }, { filter: data });
    };

    $rootScope.$watch(function watchPagerFilterForChanges () {
        return pagingService.pager.findParam;
    }, function (newValue) {
        pagingService.pager.filter.findParam = newValue;
        pagingService.pager.setFilterFindFields();
    });

    $rootScope.$watch(function watchPagerFilterForChanges () {
        return pagingService.pager.filter;
    }, function pagerFilterHasChanged (newFilter, oldFilter) {
        if (!_.isEqual(oldFilter, newFilter)) {
            $rootScope.$broadcast("pagingService:pager:filter:update", newFilter, oldFilter);
        }
    }, true);

    return pagingService;
}]);