UDApp.factory("scrapeService", ['$q', '$rootScope', '_', 'appService', 'dbService', function ($q, $rootScope, _, appService, dbService) {

    var scrapeService = {};
    
    scrapeService.addresses = {
        wikipedia: {
            collapsed: true,
            collapsedRaw: true,
            collapsedHuman: true,
            shouldFillDataFields : true,
            Raw: "", 
            Human: "",
            Json: {},
            populateItemFields: function (item) {
                var data = scrapeService.addresses.wikipedia.Json;
                scrapeService.fillItemWithData(item, data, "wikipedia");
            }
        },
        drugBank : {
            collapsed: true,
            collapsedRaw: true,
            collapsedHuman: true,
            shouldFillDataFields : true,
            Raw: "",
            Human: "",
            Json: {},
            populateItemFields: function (item) {
                var data = scrapeService.addresses.drugBank.Json;
                scrapeService.fillItemWithData(item, data, "drugBank");
            }
        }
    };
    
    scrapeService.fillItemWithData = function (item, data, dataName) {
        
        // Add image
        if (data.image) {
            item.image.text = data.image;
        }

        if (!item.data) {
            item.data = [];
        }

        var mappings = [
            {   
                key: "brandName",
                value: "clinical.brandName"
            },
            {   
                key: "mechanism.text",
                value: "pharmacology.mechanism"
            },
            {   
                key: "use.text",
                value: "pharmacology.indication" 
            },
            {   
                key: "pharmacology.absorption.text",
                value: "pharmacology.absorption"
            },
            {   
                key: "pharmacology.metabolism.text",
                value: "pharmacology.metabolism",
                concatenate: true 
            },
            {   
                key: "pharmacology.metabolism.text", 
                value: "pharmacokinetics.metabolism", 
                concatenate: true 
            },
            {   
                key: "pharmacology.excretion.text",  
                value: "pharmacokinetics.excretion" 
            },
            {
                key: "pharmacology.other.text",
                value: "pharmacokinetics.bioavailability",
                concatenate: true, 
                cb: function (value) {
                    return " Bioavailability: " + value + "\n";
                } 
            },
            {
                key: "pharmacology.other.text",
                value: "pharmacokinetics.halfLife",
                concatenate: true, 
                cb: function (value) {
                    return " Half-life:" + value + "\n";
                } 
            }
        ];

        // Add many deeply-nested values
        mappings.forEach(function (mapping) {
            var dataValue;
            try {
                dataValue   = appService.tryGetObjectDeepValue(data, mapping.value);
                keyHasValue = appService.tryGetObjectDeepValue(item, mapping.key);
                if (!keyHasValue) {
                    dataValue = mapping.cb ? mapping.cb(dataValue) : dataValue;
                    // Set the item's key with data's value
                    appService.trySetObjectDeepKeyToValue(item, {
                        key: mapping.key,
                        value: dataValue,
                        concatenate: mapping.concatenate
                    });
                }
            } catch (e) {
                console.log(e);
            }
        });
        
        // Copy all data
        item.data[dataName] = data;
    };

    // WEB SCRAPPERS
    scrapeService.scrapeWebsiteForItem = function (params, cb) {

        var item = params.item, 
            address = params.address, 
            alternativeAddress = params.alternativeAddress,
            showJsonHuman = params.showJsonHuman;

        dbService.scrapeAddress(item, address, alternativeAddress,

            function (results) {

                // console.log(results);

                if (results.error) {
                    
                    scrapeService.addresses[address].Human = "Error";
                    scrapeService.addresses[address].Raw   = "Error";

                } else {

                    var jsonResult = JSON.parse(results.data.data);
                        
                    if (showJsonHuman) {                    
                        var node = JsonHuman.format(JSON.parse(results.data.data));
                        scrapeService.addresses[address].Human = node.outerHTML;
                    }

                    // Beautified raw json, 2 spaces indentation
                    scrapeService.addresses[address].Raw   = JSON.stringify(jsonResult, undefined, 2);
                    scrapeService.addresses[address].Json  = jsonResult;

                    if (scrapeService.addresses[address].shouldFillDataFields) {
                        scrapeService.addresses[address].populateItemFields(item);
                    }

                }

                if (cb) cb(results);
            }
        );
    };

    return scrapeService;
}]);