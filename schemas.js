var _ = require('lodash');

module.exports = function (mongoose) {

    var mongoObjectId = mongoose.Types.ObjectId;

    var UserSchema = new mongoose.Schema({
        username     : { type: String, required: true, unique: true },
        email        : { type: String, required: true, unique: true }
    });

    var LinkSchema = new mongoose.Schema({
        // (name is like a 'domain name')
        name         : { type: String, required: true },
        url          : { type: String, required: true, match: /^(ht|f)tps?.*/ }
    });

    var CategorySchema = new mongoose.Schema({
        // not unique: can be named same in different category types
        name         : { type: String, required: true, unique: false },
        parent       : {
            type     : mongoose.Schema.Types.ObjectId,
            ref      : 'Category'
        },
        ancestors: [
            {
                type : mongoose.Schema.Types.ObjectId,
                ref  : 'Category'
            }
        ],
        data: {}
    });

    var CategoryTypeSchema = new mongoose.Schema({
        name         : { type: String, required: true, unique: true },
        categories   : [{
            type     : mongoose.Schema.Types.ObjectId,
            ref      : 'Category'
        }]
    });

    var DrugSchema = new mongoose.Schema({
        isProto      : { type : Boolean, default  : false },
        name         : { type : String,  required : true,  unique : true },
        // sparse allows multiple docs without field, and unique otherwise
        brandName    : { type : String,  sparse   : true, unique : true  },
        mechanism    : { text : { type : String }, likeProto : { type : Boolean, default : false } },
        use          : { text : { type : String }, likeProto : { type : Boolean, default : false } },
        sideEffects  : { text : { type : String }, likeProto : { type : Boolean, default : false } },
        other        : { text : { type : String }, likeProto : { type : Boolean, default : false } },
        image        : { text : { type : String }, likeProto : { type : Boolean, default : false } },
        parents : [
            {
                parent : {
                    type : mongoose.Schema.Types.ObjectId,
                    ref  : 'Category'
                },
                categoryType : {
                    type : mongoose.Schema.Types.ObjectId,
                    ref  : 'CategoryType'
                },
                ancestors : [
                    {
                        type : mongoose.Schema.Types.ObjectId,
                        ref  : 'Category'
                    }
                ]
            }
        ],
        pharmacology : {
            absorption   : { text: { type : String }, likeProto : { type : Boolean, default : false } },
            distribution : { text: { type : String }, likeProto : { type : Boolean, default : false } },
            metabolism   : { text: { type : String }, likeProto : { type : Boolean, default : false } },
            excretion    : { text: { type : String }, likeProto : { type : Boolean, default : false } },
            other        : { text: { type : String }, likeProto : { type : Boolean, default : false } }
        },
        links : [LinkSchema],
        data  : {},
        disabled     : { type : Boolean, default  : false }
    });

    (function () {

        var root = this, 
            SchemaHelper;

        SchemaHelper = function (collections) {
            this.collections = collections;
            this.setupGeneratableSchemaHelperFunction();
        };
        
        SchemaHelper.prototype.getMongooseObjectData = function (MongooseObject, params, res) {
            
            var execFunction = function (err, docs) {

                // findOne record - returns only 1 record
                if (params.findOne && docs) docs = docs[0];

                // populate response locals
                if (res && params.localsName) {
                    res.locals[params.localsName] = docs;
                }

                console.log(("PARAMS: " + JSON.stringify(params)).red, (params.localsName).blue);

                if (params.cb) {
                    // would call a callback with data
                    params.cb(err, docs); 
                }
            };

            if (params.lean) {
                MongooseObject
                    .find(params.findParams   || {})
                    .sort(params.sortParams   || {_id: 1})
                    .skip(params.skipParams   != null ? params.skipParams  : 0)
                    .limit(params.limitParams != null ? params.limitParams : 1000)
                    .lean()
                    .exec(execFunction);
            } else {
                MongooseObject
                    .find(params.findParams   || {})
                    .sort(params.sortParams   || {_id: 1})
                    .skip(params.skipParams   != null ? params.skipParams  : 0)
                    .limit(params.limitParams != null ? params.limitParams : 1000)
                    .exec(execFunction);
            }

        };

        SchemaHelper.prototype.setupGeneratableSchemaHelperFunction = function () {

            var self = this;

            // SchemaHelper.collections
            self.collections.forEach(function (i) {

                var capitalizedName = i.name[0].toUpperCase() + i.name.substring(1);

                // get[COLLECTION_NAME]Async
                // returns a callback with collection docs found by parameters that are passed
                SchemaHelper.prototype["get" + capitalizedName + "Async"] = 
                    function (params, res) {

                        var callParams = _.extend({
                            localsName  : i.name,   // drugs, users, etc
                            findOne     : false
                        }, params);

                        self.getMongooseObjectData(
                            i.collection,
                            callParams,
                            res
                        );
                };

                // get[COLLECTION_NAME]ById
                // returns documents in collection by its id
                SchemaHelper.prototype["get" + capitalizedName + "ById"] = function (id, params) {

                    if (id && id.toString) id = id.toString();

                    var callParams = _.extend({
                        findParams  : { _id: new mongoObjectId(id) }, 
                        findOne     : false,
                        lean        : true
                    }, params);

                    self["get" + capitalizedName + "Async"](callParams, null);
                };

                SchemaHelper.prototype.getMongoId = function (id, cb) {
                    var mongoId;
                    try {
                        mongoId = new mongoObjectId(id);
                        cb(null, mongoId);
                    } catch (e) {
                        cb(e);
                    }
                };

            });
        };

        root.SchemaHelper = SchemaHelper;

    }).call(this);


    var Schemas = {
        User         : mongoose.model('User',           UserSchema),
        CategoryType : mongoose.model('CategoryType',   CategoryTypeSchema),
        Category     : mongoose.model('Category',       CategorySchema),
        Drug         : mongoose.model('Drug',           DrugSchema),
        Link         : mongoose.model('Link',           LinkSchema)
    };

    var schemaHelper = new this.SchemaHelper(
        [
            { collection: Schemas.User,         name : 'users'        },
            { collection: Schemas.Link,         name : 'links'        },
            { collection: Schemas.CategoryType, name : 'categoryTypes'},
            { collection: Schemas.Category,     name : 'categories'   },
            { collection: Schemas.Drug,         name : 'drugs'        }
        ]
    );

    return {
        User         : Schemas.User,
        Link         : Schemas.Link,
        Category     : Schemas.Category,
        CategoryType : Schemas.CategoryType,
        Drug         : Schemas.Drug,
        SchemaHelper : schemaHelper
    };

};